# Dotfiles

These configuration files are meant to be managed with GNU stow. To install
them and check whether all dependencies are installed, the `install.sh` script
can be used.

## Dependencies

- [GNU stow](https://www.gnu.org/software/stow/) (for installation)
- [Starship](https://starship.rs/) (prompt)
- [`nvchad`](https://nvchad.com/) (base NVim config)
- [`joshuto`](https://github.com/kamiyaa/joshuto) (terminal file browser)
- [`zoxide`](https://github.com/ajeetdsouza/zoxide) (optional `cd` replacement)
- [`eza`](https://github.com/eza-community/eza) (optional `ls` replacement)
- [`bat`](https://github.com/sharkdp/bat) (optional `cat` replacement)
- [`ripgrep`](https://github.com/BurntSushi/ripgrep) (optional)

