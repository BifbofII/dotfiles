#!/usr/bin/bash
rsync -av --delete --progress --password-file=/home/christoph/.RsyncCredentials --exclude={'christoph/.cache','christoph/.log'} {/home/christoph,/mnt/Storage/{Documents,Dropbox,Movies,Music,Pictures,Videos,VirtualBox\ VMs}} rsync://christoph@jabsserver.net/ChristophDesktopBackup
