#!/usr/bin/bash
rsync -av --delete --progress --password-file=/home/christoph/.RsyncCredentials /home/christoph/{Documents,Downloads,Dropbox,Git,Music,Pictures,Public,snap,Templates,Videos,Desktop} rsync://christoph@jabsserver.net/ChristophLaptopBackup
