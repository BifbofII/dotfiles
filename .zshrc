# Set any of these to false (typically in ~/.localrc) to disable part of the config
ZSH_PLUGINS=true

[ -f ~/.localrc ] && source ~/.localrc

# Initialize autocomplete
autoload -U compinit add-zsh-hook
compinit -u

# Set Editor
if type nvim &> /dev/null; then
  export EDITOR='nvim'
  export GIT_EDITOR='nvim'
else
  export EDITOR='vim'
  export GIT_EDITOR='vim'
fi

# Disable ranger default conf
export RANGER_LOAD_DEFAULT_RC=FALSE

# Add local bin dir to path
PATH="${HOME}/.local/bin:${PATH}"
# Add cargo executables to path
PATH="${HOME}/.cargo/bin:${PATH}"

if ${ZSH_PLUGINS}; then
  if [[ ! -f $HOME/.local/share/zinit/zinit.git/zinit.zsh ]]; then
      print -P "%F{33} %F{220}Installing %F{33}ZDHARMA-CONTINUUM%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
      command mkdir -p "$HOME/.local/share/zinit" && command chmod g-rwX "$HOME/.local/share/zinit"
      command git clone https://github.com/zdharma-continuum/zinit "$HOME/.local/share/zinit/zinit.git" && \
          print -P "%F{33} %F{34}Installation successful.%f%b" || \
          print -P "%F{160} The clone has failed.%f%b"
  fi

  source "$HOME/.local/share/zinit/zinit.git/zinit.zsh"
  autoload -Uz _zinit
  (( ${+_comps} )) && _comps[zinit]=_zinit

  # Plugins
  zinit light ohmyzsh/ohmyzsh
  zinit snippet OMZP::vi-mode
  zinit snippet OMZP::rust
  zinit snippet OMZP::command-not-found

  zinit light zsh-users/zsh-completions
  zinit light zsh-users/zsh-autosuggestions
  zinit light zsh-users/zsh-syntax-highlighting
fi

# FZF config
# Arch
if [ -f /usr/share/fzf/completion.zsh ]; then source /usr/share/fzf/completion.zsh; fi
if [ -f /usr/share/fzf/key-bindings.zsh ]; then source /usr/share/fzf/key-bindings.zsh; fi
# Debian
if [ -f /usr/share/doc/fzf/examples/completion.zsh ]; then source /usr/share/doc/fzf/examples/completion.zsh; fi
if [ -f /usr/share/doc/fzf/examples/key-bindings.zsh ]; then source /usr/share/doc/fzf/examples/key-bindings.zsh; fi

if type starship &> /dev/null; then
  eval "$(starship init zsh)"
fi

# Aliases
if type zoxide &> /dev/null; then
  eval "$(zoxide init --cmd cd zsh)"
fi
if type eza &> /dev/null; then
  alias ls="eza"
  alias ll="eza -la"
  alias tree="eza -T"
fi
if type bat &> /dev/null; then
  alias cat="bat -p"
fi
type nvim &> /dev/null && alias vim="nvim"
# Enable colours for grep
alias grep='grep --color=auto'
# IP addresses
alias gip="dig +short myip.opendns.com @resolver1.opendns.com"
alias lip="ip addr | perl -pe 's:(\d+\.\d+\.\d+\.\d+):\033[31;1m$&\033[30;0m:g'"

. "$HOME/.cargo/env"
