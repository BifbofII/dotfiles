" ┌─────────────────────┐
" │ Vim Config For Java │
" └─────────────────────┘

" Mappings for Eclim Plugin
" Run Program
nnoremap <leader>r :Java<cr>
" Rename function/variable
nnoremap <leader>cw :JavaRename<Space>
" Format code
nnoremap <leader>f :%JavaFormat<cr>
" Create getter
nnoremap <leader>mg :JavaGet<cr>
" Create setter
nnoremap <leader>ms :JavaSet<cr>
" Create getter and setter
nnoremap <leader>mgs :JavaGetSet<cr>
" Create constructor
nnoremap <leader>mc :JavaConstructor<cr>
vnoremap <leader>mc :JavaConstructor<cr>
" Move class to different package
nnoremap <leader>mv :JavaMove<Space>
" Show possible corrections
nnoremap <leader>sc :JavaCorrect<Space>
" Organize imports
nnoremap <leader>ia :JavaImportOrganize<cr>
" Create new class etc.
nnoremap <leader>n :JavaNew<Space>
" Comment for java doc
nnoremap <leader>dc :JavaDocComment<cr>
" Show implementable methods
nnoremap <leader>si :JavaImpl<cr>
