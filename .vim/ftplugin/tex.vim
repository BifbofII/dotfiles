" ┌──────────────────────┐
" │ Vim Config For LaTeX │
" └──────────────────────┘

" English Spellcheck
setlocal spell spelllang=en_gb
" Compile Document
nnoremap <leader>c :!pdflatex '%'<cr>
" Open output
nnoremap <leader>r :!zathura '%:p:r.pdf'&<cr><cr>
