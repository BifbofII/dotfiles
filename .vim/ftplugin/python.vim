" ┌───────────────────────┐
" │ Vim Config For Python │
" └───────────────────────┘

" Set 4 spaces indent
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
" Execute script
nnoremap <leader>r :!python '%'<cr>
