" ┌─────────────────────────┐
" │ Vim Config For Markdown │
" └─────────────────────────┘

" Compile document
nnoremap <leader>c :Pandoc pdf<cr>
" Open output
nnoremap <leader>r :!zathura '%:p:r.pdf'&<cr><cr>
