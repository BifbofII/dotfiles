" ┌──────────────────────────┐
" │ Vim Config For C and C++ │
" └──────────────────────────┘

" Compile Code
nnoremap <leader>c :!make<cr>
