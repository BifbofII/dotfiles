" Vim syntax file
" Language: DIMACS MCNF

if exists("b:current_syntax") 
  finish
endif

syn match mcnfDelimiter " 0$"
syn match mcnfLiteral "-\?[1-9]\d*" contained nextgroup=mcnfLiteral,mcnfDelimiter skipwhite
syn match mcnfWeight "\d*" contained nextgroup=mcnfLiteral skipwhite
syn match mcnfHard "^h" nextgroup=mcnfLiteral skipwhite
syn match mcnfSoft "^o" nextgroup=mcnfWeight
syn match mcnfComment "^c.*$"

let b:current_syntax = "mcnf"

hi def link mcnfComment   Comment
hi def link mcnfLiteral   Identifier
hi def link mcnfHard      Keyword
hi def link mcnfSoft      Keyword
hi def link mcnfDelimiter Delimiter
hi def link mcnfWeight    Number
