" Vim syntax file
" Language: DIMACS WCNF

if exists("b:current_syntax") 
  finish
endif

syn match wcnfLiteral "-\?[1-9]\d*" contained nextgroup=wcnfLiteral,wcnfDelimiter skipwhite
syn match wcnfDelimiter " 0$"
syn match wcnfSoft "^\d*" nextgroup=wcnfLiteral skipwhite
syn match wcnfHard "^h" nextgroup=wcnfLiteral skipwhite
syn match wcnfComment "^c.*$"

let b:current_syntax = "wcnf"

hi def link wcnfComment   Comment
hi def link wcnfLiteral   Identifier
hi def link wcnfHard      Keyword
hi def link wcnfDelimiter Delimiter
hi def link wcnfSoft      Number
