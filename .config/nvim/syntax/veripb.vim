" Vim syntax file
" Language: VeriPB pseudo-Boolean proof

if exists("b:current_syntax") 
  finish
endif

syn match veripbBegin "pseudo-Boolean proof"
syn match veripbEnd "end pseudo-Boolean proof"
syn keyword veripbRules f e rup pol red dom def_order load_order del core sol solx solu
syn match veripbGte ">="
syn match veripbMaps "->"
syn match veripbDelimiter ";"
syn match veripbComment "^\*.*$"

let b:current_syntax = "veripb"

hi def link veripbComment   Comment
hi def link veripbRules     Keyword
hi def link veripbBegin     Statement
hi def link veripbEnd       Statement
hi def link veripbGte       Operator
hi def link veripbMaps      Operator
hi def link veripbDelimiter Delimiter
