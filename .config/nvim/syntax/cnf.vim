" Vim syntax file
" Language: DIMACS CNF

if exists("b:current_syntax") 
  finish
endif

syn match cnfHeader "^p cnf \d* \d*$"
syn match cnfLiteral "-\?[1-9]\d*"
syn match cnfComment "^c.*$"
syn match cnfDelimiter " 0$"

let b:current_syntax = "cnf"

hi def link cnfComment   Comment
hi def link cnfLiteral   Identifier
hi def link cnfHeader    Special
hi def link cnfDelimiter Delimiter
