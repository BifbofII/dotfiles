" Vim syntax file
" Language: OPB

if exists("b:current_syntax") 
  finish
endif

syn match opbObj "min:"
syn match opbEq "="
syn match opbGte ">="
syn match opbDelimiter ";"
syn match opbVar "x\d*"
syn match opbComment "\*.*$"

let b:current_syntax = "opb"

hi def link opbComment   Comment
hi def link opbGte       Operator
hi def link opbEq        Operator
hi def link opbVar       Identifier
hi def link opbDelimiter Delimiter
