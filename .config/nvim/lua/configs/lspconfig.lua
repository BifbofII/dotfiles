local base = require("nvchad.configs.lspconfig")
local on_attach = base.on_attach
local capabilities = base.capabilities

local lspconfig = require("lspconfig")

lspconfig.clangd.setup({
	on_attach = function(client, bufnr)
		client.server_capabilities.signatureHelpProvider = false
		on_attach(client, bufnr)
	end,
	capabilities = capabilities,
})

lspconfig.ltex.setup({
	on_attach = on_attach,
	settings = {
		ltex = {
			language = "en",
			additionalrules = {
				motherTongue = "de-DE",
				enablePickyRules = true,
				languageModel = "~/.languagetool/languagemodels/",
			},
			diagnosticSeverity = {
				PASSIVE_VOICE = "hint",
				TOO_LONG_SENTENCE = "hint",
				TOO_LONG_PARAGRAPH = "hint",
				default = "information",
			},
			dictionary = {
				["en"] = { "Jeremias", "Niskanen", "Matti", "Järvisalo", "MaxSAT" },
			},
			disabledRules = {
				["en"] = {
					"EN_FOR_DE_SPEAKERS_FALSE_FRIENDS_FORMULA_FORM",
					"OXFORD_SPELLING_Z_NOT_S",
				},
			},
			latex = {
				environments = {
					CCSXML = "ignore",
					algorithmic = "ignore",
				},
				commands = {
					["\\bioptsat{}"] = "dummy",
					["\\unsat{}"] = "dummy",
					["\\sat{}"] = "dummy",
					["\\satunsat{}"] = "dummy",
					["\\unsatsat{}"] = "dummy",
					["\\msu{}"] = "dummy",
					["\\oll{}"] = "vowelDummy",
					["\\msh{}"] = "dummy",
					["\\osh{}"] = "vowelDummy",
					["\\Simpr{}"] = "dummy",
					["\\Min{}"] = "dummy",
				},
			},
			enabled = {
				"bibtex",
				"gitcommit",
				"markdown",
				"org",
				"tex",
				"restructuredtext",
				"rsweave",
				"latex",
				"quarto",
				"rmd",
				"context",
			},
		},
	},
})
