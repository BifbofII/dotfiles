local on_attach = require("nvchad.configs.lspconfig").on_attach
local capabilities = require("nvchad.configs.lspconfig").capabilities
local map = vim.keymap.set

vim.g.rustaceanvim = {
	server = {
		on_attach = function(client, bufnr)
			require("lsp-inlayhints").on_attach(client, bufnr)
			on_attach(client, bufnr)
			-- Rust Lsp mappings
			map("n", "<leader>rca", function()
				vim.cmd.RustLsp("codeAction")
			end, { desc = "Rust-specific code action" })
			map("n", "<leader>rco", function()
				vim.cmd.RustLsp("openCargo")
			end, { desc = "Open Cargo.toml" })
			map("n", "<leader>rcu", function()
				require("crates").upgrade_all_crates()
			end, { desc = "Update crates" })
			map("n", "<leader>ih", function()
				require("lsp-inlayhints").toggle()
			end, { desc = "Toggle inlay hints" })
		end,
		capabilities = capabilities,
		default_settings = {
			["rust-analyzer"] = {
				checkOnSave = {
					allFeatures = false,
					command = "clippy",
					extraArgs = { "--no-deps" },
				},
			},
		},
	},
}
