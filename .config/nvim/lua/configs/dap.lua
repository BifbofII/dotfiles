local map = vim.keymap.set

-- Mappings
map("n", "<leader>db", function()
	require("dap").toggle_breakpoint()
end, { desc = "Add breakpoint at line" })
map("n", "<leader>dc", function()
	require("dap").continue()
end, { desc = "Start or continue the debugger" })
map("n", "<leader>ds", function()
	require("dap").step_over()
end, { desc = "Debugger step over" })
map("n", "<leader>di", function()
	require("dap").step_into()
end, { desc = "Debugger step into" })
map("n", "<leader>do", function()
	require("dap").step_out()
end, { desc = "Debugger step out" })
map("n", "<leader>dp", function()
	require("dap").step_out()
end, { desc = "Debugger pause" })
map("n", "<leader>du", function()
	require("dap").up()
end, { desc = "Debugger go up in stacktrace" })
map("n", "<leader>dd", function()
	require("dap").down()
end, { desc = "Debugger go down in stacktrace" })
map("n", "<leader>dt", function()
	require("dap").terminate()
end, { desc = "Debugger terminate" })
map("n", "<leader>dui", function()
	require("dapui").toggle()
end, { desc = "Toggle debugger UI" })
map("n", "<leader>drc", function()
	if vim.fn.filereadable(".vsxode/launch.json") then
		require("dap.ext.vscode").load_launchjs(nil, { cppdbg = { "c", "cpp" }, codelldb = { "c", "cpp", "rust" } })
	end
end, { desc = "Reload debug config" })

-- Load vscode launch config
if vim.fn.filereadable(".vscode/launch.json") then
	require("dap.ext.vscode").load_launchjs(nil, {
		cppdbg = { "c", "cpp" },
		codelldb = { "c", "cpp", "rust" },
	})
end
