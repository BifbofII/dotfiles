local opts = require("nvchad.configs.treesitter")

table.insert(opts.ensure_installed, "rust")
table.insert(opts.ensure_installed, "cpp")
table.insert(opts.ensure_installed, "latex")

opts.textobjects = {
	move = {
		enable = true,
		set_jumps = true,
		goto_next_start = {
			["]m"] = "@function.outer",
		},
		goto_next_end = {
			["]M"] = "@function.outer",
		},
		goto_previous_start = {
			["[m"] = "@function.outer",
		},
		goto_previous_end = {
			["[M"] = "@function.outer",
		},
		goto_next = {
			["]s"] = "@conditional.outer",
		},
		goto_previous = {
			["[s"] = "@conditional.outer",
		},
	},
}

return opts
