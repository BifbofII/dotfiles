local M = {}

-- nvim-dap-ui, redefine based on theme colours
-- control buttons
M.DapUIPlayPause = { fg = "green" }
M.DapUIRestart = { fg = "green" }
M.DapUIStop = { fg = "red" }
M.DapUIUnavailable = { fg = "grey_fg" }
M.DapUIStepOver = { fg = "cyan" }
M.DapUIStepInto = { fg = "cyan" }
M.DapUIStepBack = { fg = "cyan" }
M.DapUIStepOut = { fg = "cyan" }
-- ui panels
M.DapUIScope = { fg = "cyan" }
M.DapUIType = { fg = "magenta" }
M.DapUIModifiedValue = { fg = "cyan", bold = true }
M.DapUIDecoration = { fg = "cyan" }
M.DapUIThread = { fg = "green" }
M.DapUIStoppedThread = { fg = "cyan" }
M.DapUISource = { fg = "magenta" }
M.DapUILineNumber = { fg = "cyan" }
M.DapUIFloatBorder = { fg = "cyan" }
M.DapUIWatchesEmpty = { fg = "red" }
M.DapUIWatchesValue = { fg = "green" }
M.DapUIWatchesError = { fg = "red" }
M.DapUIBreakpointsPath = { fg = "cyan" }
M.DapUIBreakpointsInfo = { fg = "green" }
M.DapUIBreakpointsCurrentLine = { fg = "green", bold = true }
M.DapUIBreakpointsDisabledLine = { fg = "grey_fg" }
M.DapUIWinSelect = { fg = "cyan", bold = true }

-- inlay hints
M.LspInlayHint = { fg = "grey_fg", bg = "lightbg" }

return M
