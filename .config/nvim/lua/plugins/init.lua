return {
	{
		"stevearc/conform.nvim",
		event = { "BufWritePre" },
		cmd = { "ConformInfo", "FormatDisable", "FormatEnable" },
		config = function()
			require("configs.conform")
		end,
	},
	{
		"nvim-tree/nvim-tree.lua",
		opts = {
			git = { enable = true },
		},
	},
	{
		"neovim/nvim-lspconfig",
		config = function()
			require("nvchad.configs.lspconfig").defaults()
			require("configs.lspconfig")
		end,
	},
	{
		"lvimuser/lsp-inlayhints.nvim",
		dependencies = { "neovim/nvim-lspconfig" },
		opts = {},
	},
	{
		"lervag/vimtex",
		lazy = false,
		config = function()
			vim.g.vimtex_view_method = "zathura"
		end,
	},
	{
		"Civitasv/cmake-tools.nvim",
		event = "VeryLazy",
		opts = {},
	},
	{
		"nvim-treesitter/nvim-treesitter",
		-- opts = require("configs.treesitter"),
	},
	{
		"nvim-treesitter/nvim-treesitter-textobjects",
		event = { "BufReadPost", "BufNewFile" },
		dependencies = "nvim-treesitter/nvim-treesitter",
	},
	{
		"christoomey/vim-tmux-navigator",
		lazy = false,
	},
	{
		"NeogitOrg/neogit",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"sindrets/diffview.nvim",
			"nvim-telescope/telescope.nvim",
		},
		cmd = { "Neogit" },
		opts = {
			integrations = {
				diffview = true,
				telescope = true,
			},
		},
	},
	{
		"hrsh7th/nvim-cmp",
		opts = function()
			local cmp = require("cmp")
			local M = require("nvchad.configs.cmp")
			M.mapping["<CR>"] = nil
			M.mapping["<C-CR>"] = cmp.mapping.confirm({ select = true })
			M.mapping["<S-Space>"] = cmp.mapping.complete()
			return M
		end,
	},
	{
		"folke/todo-comments.nvim",
		dependencies = { "nvim-lua/plenary.nvim" },
		ft = { "rust", "cpp", "c" },
		opts = {},
	},
	{
		"henriklovhaug/Preview.nvim",
		cmd = { "Preview" },
		config = function()
			require("preview").setup()
		end,
	},
}
