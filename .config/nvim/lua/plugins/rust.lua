return {
	{
		"mrcjkb/rustaceanvim",
		version = "^4",
		ft = { "rust" },
		dependencies = "neovim/nvim-lspconfig",
		config = function()
			require("configs.rustaceanvim")
		end,
	},
	{
		"saecki/crates.nvim",
		dependencies = { "hrsh7th/nvim-cmp" },
		event = { "BufRead Cargo.toml" },
		config = function(_, opts)
			local crates = require("crates")
			crates.setup(opts)
			require("crates.completion.cmp").setup()
			require("cmp").setup.buffer({ sources = { { name = "crates" } } })
			crates.show()
		end,
	},
}
