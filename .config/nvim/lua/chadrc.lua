---@type ChadrcConfig
local M = {}

M.ui = { theme = "onedark", hl_add = require("hl_add") }

return M
