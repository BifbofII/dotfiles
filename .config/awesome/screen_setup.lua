-- ┌──────────────────────┐
-- │ Awesome Screen Setup │
-- └──────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local gears = require("gears")
local awful = require("awful")
local beautiful = require("beautiful")
local wibox = require("wibox")

-- Get hostname
local f = io.popen ("/bin/hostname")
local hostname = f:read("*a") or ""
f:close()
hostname = string.gsub(hostname, "\n$", "")

-- Custom Widgets
local top_panels = require("components.panels.top_panels")
local side_panels = require("components.panels.side_panels")

local dpi = beautiful.xresources.apply_dpi

-- ===================================================================
-- Functionality
-- ===================================================================

-- +++++++++++++++++++++++++++++++++++++++++
-- Screen Layout
-- +++++++++++++++++++++++++++++++++++++++++

if hostname == "Christoph-desktop" then
  cmd = "xrandr --output HDMI-0 --mode 1920x1080 --pos 2560x0 --rotate right --output DP-0 --primary --mode 2560x1440 --pos 0x396 --rotate normal"
  awful.spawn(cmd)
end

-- +++++++++++++++++++++++++++++++++++++++++
-- Wallpaper
-- +++++++++++++++++++++++++++++++++++++++++

local function set_wallpaper(s)
  if beautiful.wallpaper then
    local wallpaper = beautiful.wallpaper
    -- If wallpaper is a function, call it with the screen
    if type(wallpaper) == "function" then
      wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, false)
  end
end

-- Re-set Wallpaper when a Screen's Geometry changes
screen.connect_signal("property::geometry", set_wallpaper)

-- +++++++++++++++++++++++++++++++++++++++++
-- Show/Hide Bars
-- +++++++++++++++++++++++++++++++++++++++++

function updateBarsVisibility(s)
  if s.selected_tag then
    local fullscreen = s.selected_tag.fullscreenMode

    if s.top_panel then
      -- Make Top/Side Bar invisible if fullscreen
      s.top_panel.visible = not fullscreen
    end
    if s.side_panel then
      s.side_panel.visible = not fullscreen
    end
  end
end

function maximizeSideBar(s)
  if s.selected_tag then
    if s.side_panel then
      local p = s.side_panel
      local max = s.selected_tag.maximizedMode

      if max then
        p.height = s.geometry.height - dpi(26)
        p.y = s.geometry.y + dpi(26)
        p.shape = gears.shape.rectangle
      else
        p.height = p.optimal_height
        p.shape =
          function(cr, width, height)
            gears.shape.partially_rounded_rect(cr, width, height, false, true, true, false, 12)
          end
      end
    end
  end
end

-- Update Bar Visivility on Tag Selection
tag.connect_signal("property::selected",
  function(t)
    updateBarsVisibility(t.screen)
    maximizeSideBar(t.screen)
  end
)

-- Update Bar Visibility on Fullscreen Change
client.connect_signal("property::fullscreen",
  function(c)
    if c.first_tag then
      c.first_tag.fullscreenMode = c.fullscreen
    end
    updateBarsVisibility(c.screen)
  end
)

-- Update Bar Visibility on Maximized Change
client.connect_signal("property::maximized",
  function(c)
    if c.first_tag then
      c.first_tag.maximizedMode = c.maximized
    end
    maximizeSideBar(c.screen)
  end
)

-- Reset Bar if Client is closed
client.connect_signal("unmanage",
  function(c)
    if c.fullscreen then
      c.screen.selected_tag.fullscreenMode = false
    end
    if c.maximized then
      c.screen.selected_tag.maximizedMode = false
    end
    updateBarsVisibility(c.screen)
    maximizeSideBar(c.screen)
  end
)

-- Maximize Side Bar if Layout is set to Max
tag.connect_signal("property::layout",
  function(t)
    local current_layout = awful.tag.getproperty(t, "layout")
    if current_layout == awful.layout.suit.max then
      t.maximizedMode = true
    else
      t.maximizedMode = false
    end
    maximizeSideBar(t.screen)
  end
)

-- ===================================================================
-- Screen Appearance
-- ===================================================================

awful.screen.connect_for_each_screen(
  function(s)
    set_wallpaper(s)

    s.top_panel = top_panels.main(s)
  end
)
