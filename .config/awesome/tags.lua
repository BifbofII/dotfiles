-- ┌──────────────────┐
-- │ Tags for Awesome │
-- └──────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")

-- Custom Icons
local icon = require("icons")

-- Default Applications
local apps = require("apps")

-- Get hostname
local f = io.popen ("/bin/hostname")
local hostname = f:read("*a") or ""
f:close()
hostname = string.gsub(hostname, "\n$", "")

-- ===================================================================
-- Define Tags
-- ===================================================================

second_screen_layout = awful.layout.suit.tile.right
if hostname == "Christoph-desktop" then
  second_screen_layout = awful.layout.suit.tile.bottom
end

local tags = {
  { name = "Terminal",
    type = "terminal",
    defaultApp = apps.terminal,
    screen = 1,
    autostart = false,
    icon = icon.tags.terminal,
    layout = awful.layout.suit.tile.right,
  },
  { name = "Browser",
    type = "browser",
    defaultApp = apps.browser,
    screen = 1,
    autostart = false,
    icon = icon.tags.browser,
    layout = awful.layout.suit.tile.right,
  },
  { name = "Files",
    type = "files",
    defaultApp = apps.filebrowser,
    screen = 1,
    autostart = false,
    icon = icon.tags.files,
    layout = awful.layout.suit.tile.right,
  },
  { name = "E-Mail",
    type = "mail",
    defaultApp = apps.email,
    screen = 1,
    autostart = true,
    icon = icon.tags.mail,
    layout = awful.layout.suit.tile.right,
  },
  { name = "Messaging",
    type = "messaging",
    defaultApp = apps.messenger,
    screen = 1,
    autostart = true,
    icon = icon.tags.messenger,
    layout = awful.layout.suit.tile.right,
  },
  { name = "Browser",
    type = "browser",
    defaultApp = apps.browser,
    screen = 2,
    autostart = false,
    icon = icon.tags.browser,
    layout = second_screen_layout,
  },
  { name = "Terminal",
    type = "terminal",
    defaultApp = apps.terminal,
    screen = 2,
    autostart = false,
    icon = icon.tags.terminal,
    layout = second_screen_layout,
  },
  { name = "Presentation",
    type = "presentation",
    defaultApp = apps.browser,
    screen = 1,
    autostart = false,
    icon = icon.tags.player,
    layout = awful.layout.suit.tile.right,
  },
  { name = "Music",
    type = "music",
    defaultApp = apps.musicPlayer,
    screen = 2,
    autostart = false,
    icon = icon.tags.spotify,
    layout = second_screen_layout,
  },
  { name = "Other",
    type = "other",
    defaultApp = "",
    screen = 2,
    autostart = false,
    icon = icon.tags.star,
    layout = second_screen_layout,
  },
}

-- ===================================================================
-- Additional Setup
-- ===================================================================

-- Define Tag Layouts
awful.layout.layouts = {
  awful.layout.suit.tile.right,
  awful.layout.suit.tile.bottom,
  awful.layout.suit.fair,
  awful.layout.suit.fair.horizontal,
  awful.layout.suit.spiral.dwindle,
}

-- Create Tags
for i, tag in pairs(tags) do
  local t = awful.tag.add(tag.name,
    { layout = tag.layout,
      icon = tag.icon,
      screen = screen.primary,
      defaultApp = tag.defaultApp,
      selected = i == 1,
    }
  )

  if tag.autostart then
    start_default_app(t, true)
  end
end

-- Apply Tag Settings to each Tag
screen.connect_signal("request::desktop_decoration",
  function(s)
    local first = true
    for i, tag in pairs(tags) do
      if s.index == tag.screen then
        root.tags()[i].screen = tag.screen
        root.tags()[i].selected = first
        first = false
      end
    end
  end
)

-- Remove Gaps if Layout is set to Max
tag.connect_signal("property::layout",
  function(t)
    local currentLayout = awful.tag.getproperty(t, "layout")
    if currentLayout == awful.layout.suit.max then
      t.gap = 0
    else
      t.gap = beautiful.useless_gap
    end
  end
)
