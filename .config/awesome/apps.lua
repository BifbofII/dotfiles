-- ┌──────────────────────────────┐
-- │ App Declarations for Awesome │
-- └──────────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local awful = require("awful")
local filesystem = require("gears.filesystem")

-- ===================================================================
-- App Declarations
-- ===================================================================

local apps = {
	terminal = "alacritty",
	launcher = "rofi -normal-window -show drun",
	filebrowser = function(path)
		if not path then
			return "PATH=${HOME}/.cargo/bin:${PATH} wezterm -e joshuto"
		end
		return 'PATH=${HOME}/.cargo/bin:${PATH} wezterm -e joshuto "' .. path .. '"'
	end,
	gfilebrowser = function(path)
		if not path then
			return "nemo"
		end
		return "nemo " .. path
	end,
	browser = "firefox",
	editor = "alacritty -e nvim",
	musicPlayer = "spotify",
	email = "evolution",
	lock = "light-locker-command -l",
	screenshot = "scrot -e 'mv $f ~/Pictures/Screenshots/'",
	messenger = { "firefox --new-window https://web.whatsapp.com/", "telegram-desktop" },
}

-- List of Apps to start once on Start-Up
local run_on_start_up = {
	"picom",
	"unclutter",
	"light-locker",
	"redshift",
	"xfce4-power-manager",
	"dropbox",
	"nextcloud --background",
	"${HOME}/.local/bin/set_touchpad.sh",
}

-- ===================================================================
-- Functionality
-- ===================================================================

-- Run all the apps listed in run_on_start_up when awesome starts
local function run_once(cmd)
	local findme = cmd
	local firstspace = cmd:find(" ")
	if firstspace then
		findme = cmd:sub(0, firstspace - 1)
	end
	awful.spawn.with_shell(string.format("pgrep -u $USER -w %s > /dev/null || (%s)", findme, cmd), false)
end

for _, app in ipairs(run_on_start_up) do
	run_once(app)
end

-- Start default App for current tag
function start_default_app(t, once)
	if not t then
		t = awful.screen.focused().selected_tag
	end
	if not once then
		once = false
	end
	local app = t.defaultApp

	if type(app) == "string" then
		app = { app }
	end

	for i = 1, #app do
		if once then
			awful.spawn.once(
				app[i],
				{ tag = t, placement = awful.placement.bottom_right },
				nil,
				t.name .. "Def" .. app[i]
			)
		else
			awful.spawn(app[i], { tag = t, placement = awful.placement.bottom_right })
		end
	end
end

-- Return Apps List
return apps
