# Wifi Icons

Icons from [this icon pack](https://www.flaticon.com/packs/device-2)
Converted with the following command:
```
convert $ICON.png -channel RGB -negate $ICON.png
```
