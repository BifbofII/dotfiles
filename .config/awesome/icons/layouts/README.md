# Layout Icons

Icons from [this icon pack](https://www.flaticon.com/packs/layout-28?word=layout&style_id=1223&family_id=333&group_id=1219)
Converted with the following command:
```
convert -background none -gravity center $ICON.png -extent 1024x1024 -channel RGB -negate $ICON.png
```
