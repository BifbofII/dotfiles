-- ┌───────────────────┐
-- │ Icons for Awesome │
-- └───────────────────┘

-- Define Icon Folder Directory
local dir = os.getenv("HOME") .. "/.config/awesome/icons/"

-- Return Icons
return {
  logout     = dir .. "logout.png",
  sleep      = dir .. "sleep.png",
  power      = dir .. "power.png",
  lock       = dir .. "lock.png",
  brightness = dir .. "brightness.png",
  close      = dir .. "close.svg",
  open       = dir .. "open.svg",
  restart    = dir .. "restart.png",
  network    =
    { ethernet   = dir .. "ethernet.png",
      no_network = dir .. "no-network.png",
      wifi       =
        { off        = dir .. "wifi/wifi-off.png",
          strength_0 = dir .. "wifi/wifi-0.png",
          strength_1 = dir .. "wifi/wifi-1.png",
          strength_2 = dir .. "wifi/wifi-2.png",
          strength_3 = dir .. "wifi/wifi-3.png",
          strength_4 = dir .. "wifi/wifi-4.png",
        },
    },
  battery    =
    { charge_1   = dir .. "battery/battery-1.png",
      charge_2   = dir .. "battery/battery-2.png",
      charge_3   = dir .. "battery/battery-3.png",
      charge_4   = dir .. "battery/battery-4.png",
      charge_5   = dir .. "battery/battery-5.png",
      charge_6   = dir .. "battery/battery-6.png",
      charge_7   = dir .. "battery/battery-7.png",
      charging_1 = dir .. "battery/battery-charging-1.png",
      charging_2 = dir .. "battery/battery-charging-2.png",
      charging_3 = dir .. "battery/battery-charging-3.png",
      charging_4 = dir .. "battery/battery-charging-4.png",
      charging_5 = dir .. "battery/battery-charging-5.png",
      charging_6 = dir .. "battery/battery-charging-6.png",
      charging_7 = dir .. "battery/battery-charging-7.png",
      unknown    = dir .. "battery/battery-unknown.png",
    },
  volume     =
    { on  = dir .. "volume/volume-on.png",
      off = dir .. "volume/volume-off.png",
    },
  bluetooth  =
    { on  = dir .. "bluetooth/bluetooth-on.png",
      off = dir .. "bluetooth/bluetooth-off.png",
    },
  layouts    =
    { right       = dir .. "layouts/right.png",
      bottom      = dir .. "layouts/bottom.png",
      grid_right  = dir .. "layouts/grid-right.png",
      grid_bottom = dir .. "layouts/grid-bottom.png",
      spiral      = dir .. "layouts/spiral.png",
    },
  tags =
    { terminal   = dir .. "tags/terminal.png",
      browser    = dir .. "tags/firefox.png",
      editor     = dir .. "tags/notepad.png",
      files      = dir .. "tags/folder.png",
      player     = dir .. "tags/player.png",
      spotify    = dir .. "tags/spotify.png",
      games      = dir .. "tags/videogame.png",
      mail       = dir .. "tags/mail.png",
      messenger  = dir .. "tags/messager.png",
      star       = dir .. "tags/star.png",
    },
  folders =
    { documents = dir .. "folders/folder-documents.png",
      downloads = dir .. "folders/folder-download.png",
      home      = dir .. "folders/folder-home.png",
      trash     = dir .. "folders/trash.png",
    },
  titlebar =
    { close =
        { normal = dir .. "titlebar/close_normal.svg",
          focus  = dir .. "titlebar/close_focus.svg",
          hover  =
            { normal = dir .. "titlebar/close_normal_hover.svg",
              focus  = dir .. "titlebar/close_focus_hover.svg",
            },
        },
      minimize =
        { normal = dir .. "titlebar/minimize_normal.svg",
          focus  = dir .. "titlebar/minimize_focus.svg",
          hover  =
            { normal = dir .. "titlebar/minimize_normal_hover.svg",
              focus  = dir .. "titlebar/minimize_focus_hover.svg",
            },
        },
      ontop =
        { normal =
            { inactive = dir .. "titlebar/ontop_normal_inactive.svg",
              active   = dir .. "titlebar/ontop_normal_active.svg",
            },
          focus =
            { inactive = dir .. "titlebar/ontop_focus_inactive.svg",
              active   = dir .. "titlebar/ontop_focus_active.svg",
            },
          hover  =
            { normal =
                { inactive = dir .. "titlebar/ontop_normal_inactive_hover.svg",
                  active   = dir .. "titlebar/ontop_normal_active_hover.svg",
                },
              focus =
                { inactive = dir .. "titlebar/ontop_focus_inactive_hover.svg",
                  active   = dir .. "titlebar/ontop_focus_active_hover.svg",
                },
            },
        },
      sticky =
        { normal =
            { inactive = dir .. "titlebar/sticky_normal_inactive.svg",
              active   = dir .. "titlebar/sticky_normal_active.svg",
            },
          focus =
            { inactive = dir .. "titlebar/sticky_focus_inactive.svg",
              active   = dir .. "titlebar/sticky_focus_active.svg",
            },
          hover  =
            { normal =
                { inactive = dir .. "titlebar/sticky_normal_inactive_hover.svg",
                  active   = dir .. "titlebar/sticky_normal_active_hover.svg",
                },
              focus =
                { inactive = dir .. "titlebar/sticky_focus_inactive_hover.svg",
                  active   = dir .. "titlebar/sticky_focus_active_hover.svg",
                },
            },
        },
      floating =
        { normal =
            { inactive = dir .. "titlebar/floating_normal_inactive.svg",
              active   = dir .. "titlebar/floating_normal_active.svg",
            },
          focus =
            { inactive = dir .. "titlebar/floating_focus_inactive.svg",
              active   = dir .. "titlebar/floating_focus_active.svg",
            },
          hover  =
            { normal =
                { inactive = dir .. "titlebar/floating_normal_inactive_hover.svg",
                  active   = dir .. "titlebar/floating_normal_active_hover.svg",
                },
              focus =
                { inactive = dir .. "titlebar/floating_focus_inactive_hover.svg",
                  active   = dir .. "titlebar/floating_focus_active_hover.svg",
                },
            },
        },
      maximized =
        { normal =
            { inactive = dir .. "titlebar/maximized_normal_inactive.svg",
              active   = dir .. "titlebar/maximized_normal_active.svg",
            },
          focus =
            { inactive = dir .. "titlebar/maximized_focus_inactive.svg",
              active   = dir .. "titlebar/maximized_focus_active.svg",
            },
          hover  =
            { normal =
                { inactive = dir .. "titlebar/maximized_normal_inactive_hover.svg",
                  active   = dir .. "titlebar/maximized_normal_active_hover.svg",
                },
              focus =
                { inactive = dir .. "titlebar/maximized_focus_inactive_hover.svg",
                  active   = dir .. "titlebar/maximized_focus_active_hover.svg",
                },
            },
        },
    }
}
