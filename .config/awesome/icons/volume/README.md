# Bluetooth Icons

Icons from [this icon pack](https://www.flaticon.com/packs/material-design).
Converted with the following command:
```
convert $ICON.png -channel RGB -negate $ICON.png
```
