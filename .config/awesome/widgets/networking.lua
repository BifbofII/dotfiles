-- ┌───────────────────────────────┐
-- │ Networking Widget for Awesome │
-- └───────────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local awful = require("awful")
local watch = require("awful.widget.watch")
local wibox = require("wibox")
local gears = require("gears")

-- Custom Widgets
local clickable_container = require("widgets.clickable_container")

-- Icons
local icons = require("icons").network

local dpi = require("beautiful").xresources.apply_dpi

-- ===================================================================
-- Functionality
-- ===================================================================

local connected = false
local connection = "N/A"

local networking_popup = awful.tooltip(
  { objects = { },
    mode    = "outside",
    align   = "right",
    timer_function =
      function()
        if connected then
          return "Connected to " .. connection
        else
          return "No network connection"
        end
      end,
    preferred_postitions = {"right", "left", "top", "bottom"},
    margin_leftright     = dpi(8),
    margin_topbottom     = dpi(8),
  }
)

local function parseConnection()
  awful.spawn.easy_async("nmcli dev",
    function(stdout)
      if stdout:match(" connected") then
        connected = true
        connection = stdout:match(" connected +(.-)\n")
        if connection == nil then
          connection = "N/A"
        end
      else
        connected = false
        connection = "N/A"
      end
    end
  )
end

local function update_widget(widget, stdout)
  parseConnection()
  if connected then
    -- Network Connection present
    local wifi_strength = tonumber(stdout)
    if wifi_strength ~= nil then
      -- Wifi Connection
      local wifi_strength_rounded = math.floor(wifi_strength / 20)
      if wifi_strength_rounded == 0 then
        widget_icon = icons.wifi.strength_0
      elseif wifi_strength_rounded == 1 then
        widget_icon = icons.wifi.strength_1
      elseif wifi_strength_rounded == 2 then
        widget_icon = icons.wifi.strength_2
      elseif wifi_strength_rounded == 3 then
        widget_icon = icons.wifi.strength_3
      elseif wifi_strength_rounded >= 4 then
        widget_icon = icons.wifi.strength_4
      end
      widget.icon:set_image(widget_icon)
    else
      -- Wired Connection
      widget.icon:set_image(icons.ethernet)
    end
  else
    -- No Network Conenction
    widget.icon:set_image(icons.no_network)
  end
  collectgarbage("collect")
end

-- ===================================================================
-- Widget
-- ===================================================================

local function build_widget()
  local widget = wibox.widget
    { { id     = "icon",
        widget = wibox.widget.imagebox,
        resize = true,
      },
      layout = wibox.layout.align.horizontal,
    }

  watch("awk 'NR==3 {printf \"%3.0f\" ,($3/70)*100}' /proc/net/wireless", 5,
    function(_, stdout)
      update_widget(widget, stdout)
    end,
    widget
  )

  local widget_button = clickable_container(wibox.container.margin(widget, dpi(7), dpi(7), dpi(7), dpi(7)))

  widget_button:buttons(gears.table.join(
    awful.button({ }, 1, nil, function() awful.spawn("nm-connection-editor") end)
  ))

  networking_popup:add_to_object(widget_button)

  return widget_button
end

return build_widget
