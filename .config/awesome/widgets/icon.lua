-- ┌──────────────────┐
-- │ Icon for Awesome │
-- └──────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local wibox = require("wibox")
local base = require("wibox.widget.base")
local gtable = require("gears.table")

-- Custom Widgets
local clickable_container = require("widgets.clickable_container")

local dpi = require("beautiful").xresources.apply_dpi

-- ===================================================================
-- Widget
-- ===================================================================

local icon_item = { mt = { } }

-- +++++++++++++++++++++++++++++++++++++++++
-- Layout
-- +++++++++++++++++++++++++++++++++++++++++

function icon_item:layout(_, width, height)
  local layout = { }

  -- Add divider if present
  if self._private.icon then
    table.insert(layout, base.place_widget_at(
      self._private.imagebox,
      width / 2 - self._private.size / 2,
      height / 2 - self._private.size / 2,
      self._private.size,
      self._private.size
    ))
  end

  return layout
end

function icon_item:fit(_, width, height)
  local min = math.min(width, height)
  return min, min
end

-- +++++++++++++++++++++++++++++++++++++++++
-- Properties
-- +++++++++++++++++++++++++++++++++++++++++

-- Property Icon
function icon_item:set_icon(icon)
  self._private.icon = icon
  self._private.imagebox.image = icon
end

function icon_item:get_icon()
  return self._private.icon
end

-- Property Size
function icon_item:set_size(size)
  self._private.size = size
  self.emit_signal("widget::layout_changed")
end

function icon_item:get_size()
  return self._private.size
end

-- +++++++++++++++++++++++++++++++++++++++++
-- Constructor
-- +++++++++++++++++++++++++++++++++++++++++

local function new(icon, size)
  local ret = base.make_widget(nil, nil, { enable_properties = true })
  gtable.crush(ret, icon_item, true)
  ret._private.icon = icon
  ret._private.imagebox = wibox.widget.imagebox(icon)
  ret._private.size = size
  return ret
end

function icon_item.mt:__call(...)
  return new(...)
end

return setmetatable(icon_item, icon_item.mt)
