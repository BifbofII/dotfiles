-- ┌─────────────────────────┐
-- │ Icon Button for Awesome │
-- └─────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local wibox = require("wibox")
local gears = require("gears")

-- Custom Widgets
local clickable_container = require("widgets.clickable_container")

local dpi = require("beautiful").xresources.apply_dpi

-- ===================================================================
-- Widget
-- ===================================================================

function build(imagebox, args)
  return wibox.widget
    { wibox.widget
      { wibox.widget
        { imagebox,
          margins = dpi(6),
          widget  = wibox.container.margin,
        },
        shape  = gears.shape.circle,
        widget = clickable_container,
      },
      margins = dpi(6),
      widget  = wibox.container.margin,
    }
end

return build
