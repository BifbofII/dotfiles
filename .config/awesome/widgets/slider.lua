-- ┌────────────────────┐
-- │ Slider for Awesome │
-- └────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local wibox = require("wibox")
local base = require("wibox.widget.base")
local gears = require("gears")
local gtable = require("gears.table")

-- Custom Widgets
local clickable_container = require("widgets.clickable_container")

local dpi = require("beautiful").xresources.apply_dpi

-- ===================================================================
-- Widget
-- ===================================================================

local slider = { mt = { } }

local properties = { read_only = false }

-- +++++++++++++++++++++++++++++++++++++++++
-- Layout
-- +++++++++++++++++++++++++++++++++++++++++

function slider:layout(_, width, height)
  local layout = { }
  table.insert(layout, base.place_widget_at(self._private.progress_bar, 0, dpi(23), width, height - dpi(46)))

  if not self._private.read_only then
    table.insert(layout, base.place_widget_at(self._private.slider, 0, dpi(10), width, height - dpi(20)))
  end

  return layout
end

function slider:draw(_, cr, width, height)
  if self._private.read_only then
    self._private.slider.forced_height = 0
  end
end

function slider:fit(_, width, height)
  return width, height
end

-- +++++++++++++++++++++++++++++++++++++++++
-- Properties
-- +++++++++++++++++++++++++++++++++++++++++

-- Property Value
function slider:set_value(value)
  if self._private.value ~= value then
    self._private.value = value
    self._private.progress_bar:set_value(self._private.value)
    self._private.slider:set_value(self._private.value)
    self:emit_signal("property::value")
  end
end

function slider:get_value()
  return self._private.value
end

-- Property Read Only
function slider:set_read_only(value)
  if self._private.read_only ~= value then
    self._private.read_only = value
    self:emit_signal("property::read_only")
    self:emit_signal("widget::layout_changed")
  end
end

function slider:get_read_only()
  return self._private.read_only
end

-- +++++++++++++++++++++++++++++++++++++++++
-- Constructor
-- +++++++++++++++++++++++++++++++++++++++++

local function new(args)
  local ret = base.make_widget(nil, nil, { enable_properties = true })

  gtable.crush(ret._private, args or { })
  gtable.crush(ret, slider, true)

  ret._private.progress_bar = wibox.widget
    { max_value        = 100,
      value            = 25,
      forced_height    = dpi(6),
      paddings         = 0,
      shape            = gears.shape.rounded_rect,
      background_color = "#ffffff20",
      color            = "#fdfdfd",
      widget           = wibox.widget.progressbar,
    }

  ret._private.slider = wibox.widget
    { forced_height       = dpi(8),
      bar_shape           = gears.shape.rounded_rect,
      bar_height          = 0,
      bar_color           = "#fdfdfd",
      handle_color        = "#ffffff",
      handle_shape        = gears.shape.circle,
      handle_border_color = "#00000012",
      handle_border_width = dpi(3),
      value               = 25,
      widget              = wibox.widget.slider
    }

  ret._private.slider:connect_signal('property::value',
    function()
      ret:set_value(ret._private.slider.value)
    end
  )

  ret._private.read_only = false

  return ret
end

function slider.mt:__call(...)
  return new(...)
end

return setmetatable(slider, slider.mt)
