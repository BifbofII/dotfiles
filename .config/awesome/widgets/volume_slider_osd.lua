-- ┌───────────────────────────┐
-- │ Volume Slider for Awesome │
-- └───────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local wibox = require("wibox")
local awful = require("awful")
local watch = require("awful.widget.watch")

-- Custom Widgets
local list_item = require("widgets.list_item")
local slider = require("widgets.slider")
local icon_button = require("widgets.icon_button")

-- Custom Icons
local icons = require("icons").volume

-- ===================================================================
-- Widget
-- ===================================================================

local slider_osd = wibox.widget
  { read_only = false,
    widget = slider,
  }

slider_osd:connect_signal("property::value",
  function()
    awful.spawn("amixer -D pulse sset Master " .. slider_osd.value .. "%", false)
  end
)

-- A hackish way for the OSD not to hide
-- when the user is dragging the slider
-- Slider or the handle does not have a
-- Signal that handles the 'on drag' event
-- So here we are.
slider_osd:connect_signal('button::press',
  function()
    slider_osd:connect_signal('property::value',
      function()
        toggleVolOSD(true)
      end
    )
  end
)

local icon = wibox.widget
  { image  = icons.on,
    widget = wibox.widget.imagebox,
  }

local button = icon_button(icon)

local volume_setting_osd = wibox.widget
  { button,
    slider_osd,
    widget = list_item,
  }

function UpdateVolOSD()
  awful.spawn.easy_async_with_shell("bash -c 'amixer -D pulse sget Master'",
    function(stdout)
      local mute = string.match(stdout, "%[(o%D%D?)%]")
      local volume = string.match(stdout, "%[(%d?%d?%d)%%%]")
      slider_osd:set_value(tonumber(volume))
      if mute == "off" then
        icon:set_image(icons.off)
      else
        icon:set_image(icons.on)
      end
    end,
    false)
end



return volume_setting_osd
