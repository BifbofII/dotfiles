-- ┌────────────────────────────┐
-- │ Battery Widget for Awesome │
-- └────────────────────────────┘

-------------------------------------------------
-- Battery Widget for Awesome Window Manager
-- Shows the battery status using the ACPI tool
-- More details could be found here:
-- https://github.com/streetturtle/awesome-wm-widgets/tree/master/battery-widget

-- @author Pavel Makhov
-- @copyright 2017 Pavel Makhov
-- @modified Christoph Jabs
-------------------------------------------------

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local awful = require("awful")
local watch = require("awful.widget.watch")
local wibox = require("wibox")
local gears = require("gears")

-- Custom Widgets
local clickable_container = require("widgets.clickable_container")

-- Icons
local icons = require("icons").battery

local dpi = require("beautiful").xresources.apply_dpi

-- ===================================================================
-- Functionality
-- ===================================================================

local PATH_TO_ICONS = os.getenv("HOME") .. "/.config/awesome/icons/battery/"

-- Tooltip for additional infos
local battery_popup = awful.tooltip(
  { objects             = { },
    mode                = "outside",
    align               = "left",
    preferred_positions = { "right", "left", "top", "bottom" },
  }
)

local function show_battery_warning()
  naughty.notify
    { icon          = PATH_TO_ICONS .. "battery-alert.svg",
      icon_size     = dpi(48),
      text          = "Huston, we have a problem",
      title         = "Battery is dying",
      timeout       = 5,
      hover_timeout = 0.5,
      position      = "top_right",
      width         = 248,
      urgency       = critical,
    }
end

local last_battery_check = os.time()

local function update_widget(widget, stdout)
  local battery_present = string.match(stdout, "power supply: +([a-z]+)")
  if battery_present == "yes" then
    -- Laptop
    -- Extract Information
    local state = string.match(stdout, "state: +([a-z-]+)")
    charging = true
    if state == "discharging" then
      charging = false
    end
    -- Theoretical percentage
    local percentage = string.match(stdout, "percentage: +([0-9]+)%%")
    -- Accual Percentage
    local energy = string.match(stdout, "energy: +([0-9%.]+) [a-zA-Z]+")
    local energy_full = string.match(stdout, "energy%-full: +([0-9%.]+) [a-zA-Z]+")
    local charge = math.floor(tonumber(energy) / tonumber(energy_full) * 100)
    local charge7th = math.floor(charge / 100 * 7 + 0.5)
    -- Times
    local time_full = string.match(stdout, "time to full: +(.-)\n")
    local time_empty = string.match(stdout, "time to empty: +(.-)\n")

    -- Output Information
    if charging then
      if charge7th == 0 then
        widget.icon:set_image(icons.charging_1)
      elseif charge7th == 1 then
        widget.icon:set_image(icons.charging_2)
      elseif charge7th == 2 then
        widget.icon:set_image(icons.charging_3)
      elseif charge7th == 3 then
        widget.icon:set_image(icons.charging_4)
      elseif charge7th == 4 then
        widget.icon:set_image(icons.charging_5)
      elseif charge7th == 5 then
        widget.icon:set_image(icons.charging_6)
      else
        widget.icon:set_image(icons.charging_7)
      end
    else
      if charge7th == 0 then
        widget.icon:set_image(icons.charge_1)
      elseif charge7th == 1 then
        widget.icon:set_image(icons.charge_2)
      elseif charge7th == 2 then
        widget.icon:set_image(icons.charge_3)
      elseif charge7th == 3 then
        widget.icon:set_image(icons.charge_4)
      elseif charge7th == 4 then
        widget.icon:set_image(icons.charge_5)
      elseif charge7th == 5 then
        widget.icon:set_image(icons.charge_6)
      else
        widget.icon:set_image(icons.charge_7)
      end
    end

    -- Update popup text
    local popup_text = "Battery Info:\n- Status: " .. state .. "\n- Charge left: " .. charge .. "%"
    if time_full then popup_text = popup_text .. "\n- Time to full: " .. time_full end
    if time_empty then popup_text = popup_text .. "\n- Time to empty: " .. time_empty end
    battery_popup.text = popup_text
  else
    -- Desktop
    -- Output Dummy Information
    widget.icon:set_image(icons.unknown)
    -- Update popup text
    battery_popup.text = "No battery information.\nMost likely on desktop."
  end

  collectgarbage("collect")
end

-- ===================================================================
-- Widget
-- ===================================================================

local function build_widget()
  local widget = wibox.widget
    { { id     = "icon",
        widget = wibox.widget.imagebox,
        resize = true
      },
      layout = wibox.layout.fixed.horizontal,
    }

  watch("upower -i /org/freedesktop/UPower/devices/DisplayDevice", 1,
    function(_, stdout)
      update_widget(widget, stdout)
    end,
    widget
  )

  local widget_button = clickable_container(wibox.container.margin(widget, dpi(7), dpi(7), dpi(7), dpi(7)))

  widget_button:buttons(gears.table.join(
    awful.button({ }, 1, nil, function() awful.spawn("xfce4-power-manager-settings") end),
    awful.button({ }, 3, nil, function() awful.spawn("alacritty -e upower -d") end)
  ))

  battery_popup:add_to_object(widget_button)

  return widget_button
end

return build_widget
