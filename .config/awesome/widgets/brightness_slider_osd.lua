-- ┌───────────────────────────────┐
-- │ Brightness Slider for Awesome │
-- └───────────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local wibox = require("wibox")
local awful = require("awful")
local watch = require("awful.widget.watch")

-- Custom Widgets
local list_item = require("widgets.list_item")
local slider = require("widgets.slider")
local icon_button = require("widgets.icon_button")

-- Custom Icons
local icons = require("icons")

-- ===================================================================
-- Widget
-- ===================================================================

local slider_osd = wibox.widget
  { read_only = false,
    widget = slider,
  }

slider_osd:connect_signal("property::value",
  function()
    awful.spawn("brightnessetctl set " .. math.max(slider_osd.value, 5) .. "%", false)
  end
)

-- A hackish way for the OSD not to hide
-- when the user is dragging the slider
-- Slider or the handle does not have a
-- Signal that handles the 'on drag' event
-- So here we are.
slider_osd:connect_signal('button::press',
  function()
    slider_osd:connect_signal('property::value',
      function()
        toggleBriOSD(true)
      end
    )
  end
)

function UpdateBriOSD()
  awful.spawn.easy_async_with_shell("brightnessctl -m get",
    function(stdout)
      local brightness = string.match(stdout, "(%d+)")
      slider_osd:set_value(tonumber(brightness) / 255 * 100)
    end,
    false)
end

local icon = wibox.widget
  { image  = icons.brightness,
    widget = wibox.widget.imagebox,
  }

local button = icon_button(icon)

local brightness_setting_osd = wibox.widget
  { button,
    slider_osd,
    widget = list_item,
  }

return brightness_setting_osd
