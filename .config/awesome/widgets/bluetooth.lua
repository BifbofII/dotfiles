-- ┌──────────────────────────────┐
-- │ Bluetooth Widget for Awesome │
-- └──────────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local awful = require("awful")
local watch = require("awful.widget.watch")
local wibox = require("wibox")
local gears = require("gears")

-- Icons
local icons = require("icons").bluetooth

-- Custom Widgets
local clickable_container = require("widgets.clickable_container")

local dpi = require("beautiful").xresources.apply_dpi

-- ===================================================================
-- Functionality
-- ===================================================================

local checker

local bluetooth_popup = awful.tooltip(
  { objects = { },
    mode    = "outside",
    align   = "right",
    timer_function =
      function()
        if checker ~= nil then
          return "Bluetooth is on"
        else
          return "Bluetooth is off"
        end
      end,
    preferred_positions = {"right", "left", "top", "bottom"},
  }
)

local last_bluetooth_check = os.time()

local function update_widget(widget, stdout)
  -- Check if there is a bluetooth controller
  checker = stdout:match("Controller")
  local widget_icon_name
  if checker ~= nil then
    widget_icon = icons.on
  else
    widget_icon = icons.off
  end
  widget.icon:set_image(widget_icon)
  collectgarbage("collect")
end

-- ===================================================================
-- Widget
-- ===================================================================

local function build_widget()
  local widget = wibox.widget
    { { id     = "icon",
        widget = wibox.widget.imagebox,
        resize = true,
      },
      layout = wibox.layout.align.horizontal
    }

  widget.icon:set_image(icons.off)

  watch("bluetoothctl --monitor list", 5,
    function(_, stdout)
      update_widget(widget, stdout)
    end,
    widget
  )
  
  local widget_button = clickable_container(wibox.container.margin(widget, dpi(7), dpi(7), dpi(7), dpi(7)))

  widget_button:buttons(gears.table.join(
    awful.button({ }, 1, nil, function() awful.spawn("blueman-manager") end)
  ))

  bluetooth_popup:add_to_object(widget_button)

  return widget_button
end

return build_widget
