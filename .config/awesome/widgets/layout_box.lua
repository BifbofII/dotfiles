-- ┌────────────────────────┐
-- │ Layout Box for Awesome │
-- └────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local awful = require("awful")

-- Custom Widgets
local clickable_container = require("widgets.clickable_container")

-- ===================================================================
-- Widget
-- ===================================================================

local layout_box =
  function(s)
    local widget = clickable_container(awful.widget.layoutbox(s))

    widget:buttons(awful.util.table.join(
      awful.button({ }, 1, function() awful.layout.inc(1) end),
      awful.button({ }, 3, function() awful.layout.inc(-1) end),
      awful.button({ }, 4, function() awful.layout.inc(1) end),
      awful.button({ }, 5, function() awful.layout.inc(-1) end)
    ))
    return widget
  end

return layout_box
