-- ┌────────────────────────┐
-- │ Systemtray for Awesome │
-- └────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")

-- Custom Widgets
local clickable_container = require("widgets.clickable_container")

local dpi = beautiful.xresources.apply_dpi

-- ===================================================================
-- Widget
-- ===================================================================

local PATH_TO_ICONS = os.getenv("HOME") .. "/.config/awesome/icons/"

local function build_widget(s)

  -- +++++++++++++++++++++++++++++++++++++++++
  -- Systray
  -- +++++++++++++++++++++++++++++++++++++++++

  local tray = wibox.widget.systray()
  tray.visible = false
  tray:set_horizontal(true)
  tray:set_base_size(10)
  tray.opacity = 0.3

  local container = wibox.container.margin(tray, dpi(7), dpi(7), dpi(7), dpi(7))

  -- +++++++++++++++++++++++++++++++++++++++++
  -- Toggle Button
  -- +++++++++++++++++++++++++++++++++++++++++

  local arrow = wibox.widget
    { { id     = "icon",
        widget = wibox.widget.imagebox,
        resize = true,
      },
      layout = wibox.layout.align.horizontal,
    }

  arrow.icon:set_image(PATH_TO_ICONS .. "left-arrow.svg")

  local toggle_button = clickable_container(wibox.container.margin(arrow, dpi(7), dpi(7), dpi(7), dpi(7)))

  toggle_button:buttons(gears.table.join(
    awful.button({ }, 1, nil, function() awesome.emit_signal("toggle_tray") end)
  ))

  -- +++++++++++++++++++++++++++++++++++++++++
  -- Final Widget
  -- +++++++++++++++++++++++++++++++++++++++++

  local widget = wibox.widget
    { layout = wibox.layout.fixed.horizontal,
      container,
      toggle_button,
    }

  s.systray =
    { tray = tray,
      arrow = arrow,
      widget = widget,
    }

  return widget
end

-- ===================================================================
-- Functionality
-- ===================================================================

-- Toggle Systray
awesome.connect_signal("toggle_tray",
  function()
    local systray = awful.screen.focused().systray
    if systray.tray.visible then
      systray.tray.visible = false
      systray.arrow.icon:set_image(gears.surface.load_uncached(PATH_TO_ICONS .. "left-arrow.svg"))
    else
      systray.tray.visible = true
      systray.arrow.icon:set_image(gears.surface.load_uncached(PATH_TO_ICONS .. "right-arrow.svg"))
    end
  end
)

return build_widget
