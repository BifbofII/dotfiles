-- ┌────────────────────────┐
-- │ DND Widget for Awesome │
-- └────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local naughty = require("naughty")

-- Custom Widgets
local clickable_container = require("widgets.clickable_container")

local dpi = require("beautiful").xresources.apply_dpi

-- ===================================================================
-- Functionality
-- ===================================================================

local dnd_icon = os.getenv("HOME") .. "/.config/awesome/icons/dnd.svg"
local widget_button

local dnd_popup = awful.tooltip(
  { objects = { },
    mode    = "outside",
    align   = "right",
    timer_function =
      function()
        return "Notifications are suspended"
      end,
    preferred_positions = {"right", "left", "top", "bottom"},
  }
)

naughty:connect_signal("property::suspended", function() widget_button.visible = naughty.suspended end)

-- ===================================================================
-- Widget
-- ===================================================================

local function build_widget()
  local widget = wibox.widget
    { { id     = "icon",
        widget = wibox.widget.imagebox,
        resize = true,
      },
      layout = wibox.layout.align.horizontal
    }

  widget.icon:set_image(dnd_icon)

  widget_button = clickable_container(wibox.container.margin(widget, dpi(7), dpi(7), dpi(7), dpi(7)))

  widget_button:buttons(gears.table.join(
    awful.button({ }, 1, nil, function() naughty.suspended = !naughty.suspended end)
  ))

  dnd_popup:add_to_object(widget_button)

  return widget_button
end

return build_widget
