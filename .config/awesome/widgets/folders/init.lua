-- ┌────────────────────────────┐
-- │ Folder Widgets for Awesome │
-- └────────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local wibox = require("wibox")
local beautiful = require("beautiful")

-- Custom Widgets
local documents_button = require("widgets.folders.documents")
local downloads_button = require("widgets.folders.downloads")
local home_button = require("widgets.folders.home")
local trash_button = require("widgets.folders.trash")

local dpi = beautiful.xresources.apply_dpi

-- ===================================================================
-- Return
-- ===================================================================

local separator = wibox.widget
  { orientation   = "horizontal",
    forced_height = dpi(16),
    opacity       = .2,
    widget        = wibox.widget.separator,
  }

return
  { documents = documents_button,
    downloads = downloads_button,
    home      = home_button,
    trash     = trash_button,
    separator = separator,
  }
