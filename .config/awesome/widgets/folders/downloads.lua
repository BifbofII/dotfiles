-- ┌─────────────────────────────────────┐
-- │ Downloads Folder Widget for Awesome │
-- └─────────────────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Custom Widgets
local folder_button = require("widgets.folders.base")

-- Custom Icons
local icon = require("icons").folders.downloads

-- ===================================================================
-- Widget
-- ===================================================================

local HOME = os.getenv("HOME")

return folder_button("Downloads", HOME .. "/Downloads/", icon)
