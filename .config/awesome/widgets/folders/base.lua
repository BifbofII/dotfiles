-- ┌────────────────────────────────┐
-- │ Base Folder Widget for Awesome │
-- └────────────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

-- Custom Modules
local apps = require("apps")

-- Custom Widgets
local clickable_container = require("widgets.clickable_container")

local dpi = beautiful.xresources.apply_dpi
local filebrowser = apps.filebrowser
local gfilebrowser = apps.gfilebrowser

-- ===================================================================
-- Widget
-- ===================================================================

local function build_button(name, path, icon, g_only)
  local folder_widget = wibox.widget
    { { id     = "icon",
        widget = wibox.widget.imagebox,
        resize = true,
      },
      layout = wibox.layout.align.horizontal,
    }

  folder_widget.icon:set_image(icon)

  local folder_button = clickable_container(wibox.container.margin(folder_widget, dpi(8), dpi(8), dpi(8), dpi(8)))

  if g_only then
    folder_button:buttons(gears.table.join(
      awful.button({ }, 1, nil, function() awful.spawn(gfilebrowser(path)) end),
      awful.button({ }, 3, nil, function() awful.spawn(gfilebrowser(path)) end)
    ))
  else
    folder_button:buttons(gears.table.join(
      awful.button({ }, 1, nil, function() awful.spawn(filebrowser(path)) end),
      awful.button({ }, 3, nil, function() awful.spawn(gfilebrowser(path)) end)
    ))
  end

  awful.tooltip(
    { objects = { folder_button },
      mode = "outside",
      align = "right",
      timer_function = function() return name end,
      preferred_positions = { "right", "left", "top", "bottom" },
    }
  )

  return folder_button
end

return build_button
