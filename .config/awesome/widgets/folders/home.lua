-- ┌────────────────────────────────┐
-- │ Home Folder Widget for Awesome │
-- └────────────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Custom Widgets
local folder_button = require("widgets.folders.base")

-- Custom Icons
local icon = require("icons").folders.home

-- ===================================================================
-- Widget
-- ===================================================================

local HOME = os.getenv("HOME")

return folder_button("Home", HOME, icon)
