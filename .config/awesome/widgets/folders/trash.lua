-- ┌─────────────────────────────────┐
-- │ Trash Folder Widget for Awesome │
-- └─────────────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Custom Widgets
local folder_button = require("widgets.folders.base")

-- Custom Icons
local icon = require("icons").folders.trash

-- ===================================================================
-- Widget
-- ===================================================================

local HOME = os.getenv("HOME")

return folder_button("Trashbin", "trash://", icon, true)
