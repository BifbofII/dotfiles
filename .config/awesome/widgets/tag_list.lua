-- ┌──────────────────────┐
-- │ Tag List for Awesome │
-- └──────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")

-- Custom Modules
local keys = require("keys")

-- Custom Widgets
local clickable_container = require("widgets.clickable_container")

local dpi = beautiful.xresources.apply_dpi
local modkey = keys.modkey
local capi = { button = button }

-- ===================================================================
-- Widget
-- ===================================================================

local function create_buttons(buttons, object)
  if buttons then
    local btns = { }
    for _, b in ipairs(buttons) do
      -- Create Proxy Button Object:
      -- Catch Keypresses and propagate to Button with the Object as Argument
      local btn = capi.button
        { modifiers = b.modifiers,
          button    = b.button,
        }
      btn:connect_signal("press", function() b:emit_signal("press", object) end)
      btn:connect_signal("release", function() b:emit_signal("release", object) end)
      btns[#btns+1] = btn
    end

    return btns
  end
end

local function list_update(w, buttons, label, data, objects)
  -- Update the Widgets, creating them if needed
  w:reset()
  for i, o in ipairs(objects) do
    local cache = data[o]
    local ib, tb, bgb, tbm, ibm, l, bg_clickable
    if cache then
      ib  = cache.ib
      tb  = cache.tb
      bgb = cache.bgb
      tbm = cache.tbm
      ibm = cache.ibm
    else
      local icondpi = dpi(10)
      ib  = wibox.widget.imagebox()
      tb  = wibox.widget.textbox()
      bgb = wibox.container.background()
      tbm = wibox.container.margin(tb, dpi(4), dpi(16))
      ibm = wibox.container.margin(ib, icondpi, icondpi, icondpi, icondpi)
      l   = wibox.layout.fixed.horizontal()
      bg_clickable = clickable_container()

      -- Add to fixed Widget
      l:fill_space(true)
      l:add(ibm)
      bg_clickable:set_widget(l)

      -- Set Background
      bgb:set_widget(bg_clickable)
      bgb:buttons(create_buttons(buttons,o))

      data[o] =
        { ib  = ib,
          tb  = tb,
          bgb = bgb,
          tbm = tbm,
          ibm = ibm,
        }
    end

    local text ,bg, bg_image, icon, args = label(o, tb)
    args = args or { }

    bgb:set_bg(bg)
    if type(bg_image) == "function" then
      -- TODO: Why does this pass nil as an argument?
      bg_image = bg_image(tb, o, nil, objects, i)
    end
    
    bgb:set_bgimage(bg_image)
    if icon then
      ib.image = icon
    else
      ibm:set_margins(0)
    end

    bgb.shape = args.shape
    bgb.shape_border_width = args.shape_border_width
    bgb.shape_border_color = args.shape_border_color

    w:add(bgb)
  end
end

-- Custom source function for tags to include all tags, not only the ones on the screen
local function source_all_tags(s)
  return root.tags()
end

local function tag_list(s)
  return awful.widget.taglist(
    { screen = s,
      filter = awful.widget.taglist.filter.all,
      source = source_all_tags,
      buttons = awful.util.table.join(
        awful.button({ }, 1, function(t) t:view_only() end),
        awful.button({ modkey }, 1,
          function(t)
            if client.focus then
              client.focus:move_to_tag(t)
              t:view_only()
            end
          end
        ),
        awful.button({ }, 3, awful.tag.viewtoggle),
        awful.button({ modkey }, 3,
          function(t)
            if client.focus then
              client.focus:toggle_tag(t)
            end
          end
        )
      ),
    update_function = list_update,
    layout = wibox.layout.fixed.vertical(),
  })
end

return tag_list
