-- ┌───────────────────┐
-- │ Theme for Awesome │
-- └───────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")

-- Custom Icons
local layout_icons = require("icons").layouts
local title_icons = require("icons").titlebar

local dpi = xresources.apply_dpi

local theme = { }

-- ===================================================================
-- Theme Variables
-- ===================================================================

-- Font
theme.font       = "SF Pro Text 9"
theme.title_font = "SF Pro Display Medium 10"

-- Background
theme.bg_normal   = "#1f2430"
theme.bg_dark     = "#000000"
theme.bg_focus    = "#151821"
theme.bg_urgent   = "#3f3f3f"
theme.bg_minimize = "#444444"
theme.bg_systray  = theme.bg_normal

-- Foreground
theme.fg_normal   = "#ffffff"
theme.fg_focus    = "#e4e4e4"
theme.fg_urgent   = "#cc9393"
theme.fg_minimize = "#ffffff"

-- Sizing
theme.useless_gap       = dpi(7)
theme.gap_single_client = true

-- Window Borders
theme.border_width  = dpi(0)
theme.border_normal = theme.bg_normal
theme.border_focus  = "#ff8a65"
theme.border_marked = theme.fg_urgent

-- Titlebars
theme.titlebar_font     = theme.title_font
theme.titlebar_bg       = theme.bg_normal
theme.titlebar_bg_focus = theme.titlebar_bg
theme.titlebar_size     = dpi(34)

-- Titlebar Icons
-- Close Button
theme.titlebar_close_button_normal       = title_icons.close.normal
theme.titlebar_close_button_focus        = title_icons.close.focus
theme.titlebar_close_button_normal_hover = title_icons.close.hover.normal
theme.titlebar_close_button_focus_hover  = title_icons.close.hover.focus
-- Minimize Button
theme.titlebar_minimize_button_normal       = title_icons.minimize.normal
theme.titlebar_minimize_button_focus        = title_icons.minimize.focus
theme.titlebar_minimize_button_normal_hover = title_icons.minimize.hover.normal
theme.titlebar_minimize_button_focus_hover  = title_icons.minimize.hover.focus
-- Ontop Button
theme.titlebar_ontop_button_normal_inactive       = title_icons.ontop.normal.inactive
theme.titlebar_ontop_button_focus_inactive        = title_icons.ontop.focus.inactive
theme.titlebar_ontop_button_normal_active         = title_icons.ontop.normal.active
theme.titlebar_ontop_button_focus_active          = title_icons.ontop.focus.active
theme.titlebar_ontop_button_normal_inactive_hover = title_icons.ontop.hover.normal.inactive
theme.titlebar_ontop_button_focus_inactive_hover  = title_icons.ontop.hover.focus.inactive
theme.titlebar_ontop_button_normal_active_hover   = title_icons.ontop.hover.normal.active
theme.titlebar_ontop_button_focus_active_hover    = title_icons.ontop.hover.focus.active
-- Sticky Button
theme.titlebar_sticky_button_normal_inactive       = title_icons.sticky.normal.inactive
theme.titlebar_sticky_button_focus_inactive        = title_icons.sticky.focus.inactive
theme.titlebar_sticky_button_normal_active         = title_icons.sticky.normal.active
theme.titlebar_sticky_button_focus_active          = title_icons.sticky.focus.active
theme.titlebar_sticky_button_normal_inactive_hover = title_icons.sticky.hover.normal.inactive
theme.titlebar_sticky_button_focus_inactive_hover  = title_icons.sticky.hover.focus.inactive
theme.titlebar_sticky_button_normal_active_hover   = title_icons.sticky.hover.normal.active
theme.titlebar_sticky_button_focus_active_hover    = title_icons.sticky.hover.focus.active
-- Floating Button
theme.titlebar_floating_button_normal_inactive       = title_icons.floating.normal.inactive
theme.titlebar_floating_button_focus_inactive        = title_icons.floating.focus.inactive
theme.titlebar_floating_button_normal_active         = title_icons.floating.normal.active
theme.titlebar_floating_button_focus_active          = title_icons.floating.focus.active
theme.titlebar_floating_button_normal_inactive_hover = title_icons.floating.hover.normal.inactive
theme.titlebar_floating_button_focus_inactive_hover  = title_icons.floating.hover.focus.inactive
theme.titlebar_floating_button_normal_active_hover   = title_icons.floating.hover.normal.active
theme.titlebar_floating_button_focus_active_hover    = title_icons.floating.hover.focus.active
-- Maximized Button
theme.titlebar_maximized_button_normal_inactive       = title_icons.maximized.normal.inactive
theme.titlebar_maximized_button_focus_inactive        = title_icons.maximized.focus.inactive
theme.titlebar_maximized_button_normal_active         = title_icons.maximized.normal.active
theme.titlebar_maximized_button_focus_active          = title_icons.maximized.focus.active
theme.titlebar_maximized_button_normal_inactive_hover = title_icons.maximized.hover.normal.inactive
theme.titlebar_maximized_button_focus_inactive_hover  = title_icons.maximized.hover.focus.inactive
theme.titlebar_maximized_button_normal_active_hover   = title_icons.maximized.hover.normal.active
theme.titlebar_maximized_button_focus_active_hover    = title_icons.maximized.hover.focus.active

-- Taglist
theme.taglist_bg_empty    = theme.bg_normal
theme.taglist_bg_occupied = '#ffffff1A'
theme.taglist_bg_urgent   = '#E91E6399'
theme.taglist_bg_focus    = theme.bg_focus

-- Tasklist
theme.tasklist_font      = theme.font

theme.tasklist_bg_normal = theme.bg_normal
theme.tasklist_bg_focus  = theme.bg_focus
theme.tasklist_bg_urgent = theme.bg_urgent

theme.tasklist_fg_focus  = theme.fg_focus
theme.tasklist_fg_urgent = theme.fg_urgent
theme.tasklist_fg_normal = theme.fg_normal

-- Notifications
theme.notification_max_width = dpi(500)
theme.notification_margin    = dpi(5)
theme.notification_padding   = dpi(8)

-- Menu
theme.menu_font    = theme.font
theme.menu_submenu = ''

-- Client Shape
theme.corner_radius = dpi(15)

-- Systray
theme.systray_icon_spacing = 16

-- Wallpaper
theme.wallpaper = "~/Pictures/Wallpaper"

-- ===================================================================
-- Icons
-- ===================================================================

-- Layouts
theme.layout_tile       = layout_icons.right
theme.layout_tilebottom = layout_icons.bottom
theme.layout_fairh      = layout_icons.grid_right
theme.layout_fairv      = layout_icons.grid_bottom
theme.layout_dwindle    = layout_icons.spiral

-- Return Theme
return theme
