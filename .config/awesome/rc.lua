-- ┌──────────────────────────┐
-- │ Main Awesome Config File │
-- └──────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

pcall(require, "luarocks.loader")

-- Standard Awesome Modules
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
require("awful.hotkeys_popup.keys")
local beautiful = require("beautiful")

local config_dir = gears.filesystem.get_configuration_dir()

-- Import Theme
beautiful.init(config_dir .. "/theme.lua")

-- Screen Setup
require("screen_setup")

-- Import Keybindings
local keys = require("keys")

-- Import Components
require("components.notifications") -- Contains Error Handling
require("components.exit_screen")
require("components.titlebar")
require("components.volume_osd")
require("components.brightness_osd")

-- Import Tag Settings
require("tags")

-- Import Rules
require("rules")

-- ===================================================================
-- Signals
-- ===================================================================

-- Signal Function to Execute when a new Client appears
client.connect_signal("manage",
  function(c)
    -- Set Window as Slave
    if not awesome.startup then
      awful.client.setslave(c)
    end

    if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
      -- Prevent clients from being unreachable after screen count changes
      awful.placement.no_offscreen(c)
    end
  end
)

-- Enable sloppy Focus, so that Focus follows Mouse
client.connect_signal("mouse::enter",
  function(c)
    c:emit_signal("request::activate", "mouse_enter", { raise = false })
  end
)

-- ===================================================================
-- Garbage Collection (For lower Memory Consumption)
-- ===================================================================

collectgarbage("setpause", 160)
collectgarbage("setstepmul", 400)
