-- ┌───────────────────┐
-- │ Rules for Awesome │
-- └───────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local awful = require("awful")
local beautiful = require("beautiful")
local ruled = require("ruled")

-- Keybindings
local keys = require("keys")

-- ===================================================================
-- Rules
-- ===================================================================

-- All Clients will match this Rule
ruled.client.append_rule {
  id = "general",
  rule = { },
  properties = {
    border_width = beautiful.border_width,
    border_color = beautiful.border_normal,
    focus = awful.client.focus.filter,
    raise = true,
    keys = keys.clientkeys,
    buttons = keys.clientbuttons,
    screen = awful.screen.preferred,
    placement = awful.placement.no_overlap + awful.placement.no_offscreen + awful.placement.centered,
  }
}

-- Floating Clients
ruled.client.append_rule {
  id = "floating",
  rule_any = {
    instance = {
      "DTA", -- Firefox Addon DownThemAll
      "copyq", -- Inclused Session Name in Class
      "pinentry",
    },
    class = {
      "Arandr",
      "Nm-connection-editor",
      "Blueman-manager",
    },
    name = {
      "Event Tester",
      "Steam Guard - Computer Authorization Required",
    },
    role = {
      "pop-up",
      "GtkFileChooserDialog",
    },
    type = {
      "dialog",
      "splash",
    }
  },
  properties = { floating = true },
}

-- Messenger
ruled.client.append_rule {
  id = "messenger",
  rule_any = {
    instance = {
      "telegram-desktop",
      "element",
    },
  },
  properties = { tag = awful.tag.find_by_name(nil, "Messaging") },
}

-- Rofi
ruled.client.append_rule {
  id = "rofi",
  rule_any = { class = { "Rofi" } },
  properties = { fullscreen = true, ontop = true, request_no_titlebar = true },
}

-- Songbeamer
ruled.client.append_rule {
  id = "songbeamer_main",
  rule = { instance = "songbeamer.exe" },
  properties = { maximized = false },
}

-- Zoom
ruled.client.append_rule {
  id = "zoom",
  rule = {
    class = "zoom ",
    above = true,
  },
  properties = { floating = true },
}