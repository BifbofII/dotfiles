-- ┌─────────────────────────┐
-- │ Keybindings for Awesome │
-- └─────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")
local beautiful = require("beautiful")

-- Default Applications
local apps = require("apps")

-- Define Modifiers
local modkey = "Mod4"
local altkey = "Mod1"

local keys = { }

-- ===================================================================
-- Mouse Bindings
-- ===================================================================

-- Mouse buttons on the desktop
keys.desktopbuttons = gears.table.join(
  -- Left Click on Desktop to hide Notifications
  awful.button({ }, 1, naughty.destroy_all_notifications)
)

keys.clientbuttons = gears.table.join(
  -- Select client with click
  awful.button({ }, 1,
    function(c)
      c:emit_signal("request::activate", "mouse_click", { raise = true })
    end
  ),
  -- Move client with Mod+Click
  awful.button({ modkey }, 1,
    function(c)
      c:emit_signal("request::activate", "mouse_click", { raise = true })
      awful.mouse.client.move(c)
    end
  ),
  -- Resize client with Mod+Right Click
  awful.button({ modkey }, 3,
    function (c)
      c:emit_signal("request::activate", "mouse_click", { raise = true })
      awful.mouse.client.resize(c)
    end
  )
)

-- ===================================================================
-- Key Bindings
-- ===================================================================

keys.globalkeys = gears.table.join(
  -- +++++++++++++++++++++++++++++++++++++++++
  -- Application Key Bindings
  -- +++++++++++++++++++++++++++++++++++++++++

  -- Spawn Terminal
  awful.key({ modkey }, "Return",
    function()
      awful.spawn(apps.terminal)
    end,
    { description = "open a terminal", group = "launcher" }
  ),
  -- Spawn Launcher
  awful.key({ modkey }, "d",
    function()
      awful.spawn(apps.launcher)
    end,
    { description = "application launcher", group = "launcher" }
  ),
  -- Spawn File Manager
  awful.key({ modkey }, "e",
    function()
      awful.spawn.with_shell(apps.filebrowser())
    end,
    { description = "filebrowser", group = "launcher" }
  ),
  awful.key({ modkey, "Shift" }, "e",
    function()
      awful.spawn(apps.gfilebrowser())
    end,
    { description = "graphical filebrowser", group = "launcher" }
  ),
  -- Spawn Default App
  awful.key({ modkey }, "s",
    function()
      start_default_app()
    end,
    { description = "tag default app", group = "launcher" }
  ),

  -- +++++++++++++++++++++++++++++++++++++++++
  -- Volume / Brightness / Screenshot
  -- +++++++++++++++++++++++++++++++++++++++++

  -- Brightness
  awful.key({ }, "XF86MonBrightnessUp",
    function()
      awful.spawn("brightnessctl set +5%")
      if toggleBriOSD ~= nil then
        toggleBriOSD(true)
      end
      if UpdateBriOSD ~= nil then
        UpdateBriOSD()
      end
    end,
    { description = "+5% brightness", group = "hotkeys" }
  ),
  awful.key({ }, "XF86MonBrightnessDown",
    function()
      awful.spawn("brightnessctl set 5%-")
      if toggleBriOSD ~= nil then
        toggleBriOSD(true)
      end
      if UpdateBriOSD ~= nil then
        UpdateBriOSD()
      end
    end,
    { description = "-5% brightness", group = "hotkeys" }
  ),

  -- ALSA Volume Control
  awful.key({ }, "XF86AudioRaiseVolume",
    function()
      awful.spawn("amixer -D pulse sset Master 5%+")
      if toggleVolOSD ~= nil then
        toggleVolOSD(true)
      end
      if UpdateVolOSD ~= nil then
        UpdateVolOSD()
      end
    end,
    { description = "volume up", group = "hotkeys" }
  ),
  awful.key({ }, "XF86AudioLowerVolume",
    function()
      awful.spawn("amixer -D pulse sset Master 5%-")
      if toggleVolOSD ~= nil then
        toggleVolOSD(true)
      end
      if UpdateVolOSD ~= nil then
        UpdateVolOSD()
      end
    end,
    { description = "volume down", group = "hotkeys" }
  ),
  awful.key({ }, "XF86AudioMute",
    function()
      awful.spawn("amixer -D pulse set Master 1+ toggle")
      if toggleVolOSD ~= nil then
        toggleVolOSD(true)
      end
      if UpdateVolOSD ~= nil then
        UpdateVolOSD()
      end
    end,
    { description = "toggle mute", group = "hotkeys" }
  ),
  awful.key({ }, "XF86AudioNext",
    function()
      awful.spawn("playerctl next --ignore-player=firefox")
    end,
    { description = "next music", group = "hotkeys" }
  ),
  awful.key({ }, "XF86AudioPrev",
    function()
      awful.spawn("playerctl previous --ignore-player=firefox")
    end,
    { description = "previous music", group = "hotkeys" }
  ),
  awful.key({ }, "XF86AudioPlay",
    function()
      awful.spawn("playerctl play-pause --ignore-player=firefox")
    end,
    { description = "play/pause music", group = "hotkeys" }
  ),

  -- Screenshot
  awful.key({ modkey, "Shift" }, "s",
    function()
      awful.util.spawn(apps.screenshot, false)
    end,
    { description = "take a screenshot", group = "hotkeys" }
  ),

  -- +++++++++++++++++++++++++++++++++++++++++
  -- Client Focusing
  -- +++++++++++++++++++++++++++++++++++++++++

  -- Focus client by direction (vimlike)
  awful.key({ modkey }, "j",
    function()
      awful.client.focus.bydirection("down")
      if client.focus then client.focus:raise() end
    end,
    { description = "focus down", group = "client" }
  ),
  awful.key({ modkey }, "k",
    function()
      awful.client.focus.bydirection("up")
      if client.focus then client.focus:raise() end
    end,
    { description = "focus up", group = "client" }
  ),
  awful.key({ modkey }, "h",
    function()
      awful.client.focus.bydirection("left")
      if client.focus then client.focus:raise() end
    end,
    { description = "focus left", group = "client" }
  ),
  awful.key({ modkey }, "l",
    function()
      awful.client.focus.bydirection("right")
      if client.focus then client.focus:raise() end
    end,
    { description = "focus right", group = "client" }
  ),

  -- +++++++++++++++++++++++++++++++++++++++++
  -- Reload/Quit Awesome
  -- +++++++++++++++++++++++++++++++++++++++++

  -- Reload Awesome
  awful.key({ modkey, "Shift" }, "r",
    awesome.restart,
    { description = "reload awesome", group = "awesome" }
  ),

  -- Quit Awesome
  awful.key({ modkey, "Shift" }, "BackSpace",
    function()
      exit_screen_show()
    end,
    { description = "quit awesome", group = "awesome" }
  ),
  awful.key({ }, "XF86PowerOff",
    function()
      exit_screen_show()
    end,
    { description = "quit awesome", group = "awesome" }
  ),

  -- +++++++++++++++++++++++++++++++++++++++++
  -- Gap Control
  -- +++++++++++++++++++++++++++++++++++++++++

  -- Increase Gap
  awful.key({ modkey }, "plus",
    function()
      awful.tag.incgap(5, nil)
    end,
    { description = "increase gap size for the current tag", group = "gaps" }
  ),

  -- Descrease Gap
  awful.key({ modkey }, "minus",
    function()
      awful.tag.incgap(-5, nil)
    end,
    { description = "decrease gap size for the current tag", group = "gaps" }
  ),

  -- Toggle focus mode
  awful.key({ modkey, "Shift" }, "h",
    function()
      local tag = awful.screen.focused().selected_tag
      if tag.gap == 0 then
        tag.gap = beautiful.useless_gap
      else
        tag.gap = 0
      end
    end,
    { description = "toggle focus mode: set gaps to 0 / default", group = "gaps" }
  ),

  -- +++++++++++++++++++++++++++++++++++++++++
  -- Layout Selection
  -- +++++++++++++++++++++++++++++++++++++++++

  -- Select Next Layout
  awful.key({ modkey }, "space",
    function()
      awful.layout.inc(1)
    end,
    { description = "select next", group = "layout" }
  ),

  -- Select Previous Layout
  awful.key({ modkey, "Shift" }, "space",
    function()
      awful.layout.inc(-1)
    end,
    { description = "select previous", group = "layout" }
  ),

  -- +++++++++++++++++++++++++++++++++++++++++
  -- Client Control
  -- +++++++++++++++++++++++++++++++++++++++++
  
  -- Restore Minimized Client
  awful.key({ modkey, "Shift" }, "n",
    function()
      local c = awful.client.restore()
      if c then
        c:emit_signal(
          "request::activate", "key.unminimize", { raise = true }
        )
      end
    end,
    { description = "restore minimized", group = "client" }
  ),

  -- +++++++++++++++++++++++++++++++++++++++++
  -- Multiple screens
  -- +++++++++++++++++++++++++++++++++++++++++

  -- Move client between screens
  awful.key({ modkey, "Shift" }, "s",
    function()
      local c = client.focus
      if c then
        c:move_to_screen()
      end
    end,
    { description = "move tag to next screen", group = "tag" }
  ),

  -- +++++++++++++++++++++++++++++++++++++++++
  -- Notifications
  -- +++++++++++++++++++++++++++++++++++++++++

  -- Toggle notifications
  awful.key({ modkey, "Shift" }, "m",
    function()
      naughty.suspended = not naughty.suspended
    end,
    { description = "toggle notifications", group = "notifications" }
  )
)

keys.clientkeys = gears.table.join(
  -- +++++++++++++++++++++++++++++++++++++++++
  -- Client Keys
  -- +++++++++++++++++++++++++++++++++++++++++
  -- Toggle Fullscreen
    awful.key({ modkey }, "f",
      function (c)
        c.fullscreen = not c.fullscreen
        c:raise()
      end,
      { description = "toggle fullscreen", group = "client" }
    ),

  -- Toggle Floating
    awful.key({ modkey, "Shift" }, "f",
      function (c)
        c.floating = not c.floating
      end,
      { description = "toggle floating", group = "client" }
    ),

  -- Close Client
  awful.key({ modkey, "Shift" }, "q",
    function(c)
      c:kill()
    end,
    { description = "close", group = "client" }
  ),

  -- Minimize
  awful.key({ modkey }, "n",
    function(c)
      c.minimized = true
    end,
    { description = "minimize", group = "client" }
  ),

  -- Maximize
  awful.key({ modkey }, "m",
    function (c)
      c.maximized = not c.maximized
      c:raise()
    end ,
    { description = "(un)maximize", group = "client" }
  )
)

-- +++++++++++++++++++++++++++++++++++++++++
-- Key Numbers to Tags (Screen independent)
-- +++++++++++++++++++++++++++++++++++++++++
for i = 1, 10 do
  keys.globalkeys = gears.table.join(keys.globalkeys,
    -- View tag only.
    awful.key({ modkey }, "#" .. i + 9,
      function ()
        local tag = root.tags()[i]
        if tag then
          tag:view_only()
          awful.screen.focus(tag.screen)
        end
      end,
      { description = "view tag #"..i, group = "tag" }
    ),
    -- Toggle tag display.
    awful.key({ modkey, "Control" }, "#" .. i + 9,
      function ()
        local tag = root.tags()[i]
        if tag then
          awful.tag.viewtoggle(tag)
        end
      end,
      { description = "toggle tag #" .. i, group = "tag" }
    ),
    -- Move client to tag.
    awful.key({ modkey, "Shift" }, "#" .. i + 9,
      function ()
        if client.focus then
          local tag = root.tags()[i]
          if tag then
            client.focus:move_to_tag(tag)
          end
        end
      end,
      { description = "move focused client to tag #"..i, group = "tag" }
      ),
    -- Toggle tag on focused client.
    awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
      function ()
        if client.focus then
          local tag = root.tags()[i]
          if tag then
            client.focus:toggle_tag(tag)
          end
        end
      end,
      { description = "toggle focused client on tag #" .. i, group = "tag" }
    )
  )
end

-- ===================================================================
-- Set Keys
-- ===================================================================

root.keys(keys.globalkeys)
root.buttons(keys.desktopbuttons)

return keys
