-- ┌────────────────────────┐
-- │ Volume OSD for Awesome │
-- └────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")

-- Custom Widgets
local vol_osd = require("widgets.volume_slider_osd")

local dpi = beautiful.xresources.apply_dpi

-- ===================================================================
-- Add Volume Overlay to Each Screen
-- ===================================================================

volumeOverlays = {}

screen.connect_signal("request::desktop_decoration",
  function(s)
    -- Create Box
    local offsetx = dpi(56, s)
    local offsety = dpi(300, s)

    volumeOverlays[s.index] = wibox
      { visible = nil,
        screen  = s,
        ontop   = true,
        type    = "normal",
        height  = offsety,
        width   = dpi(48, s),
        bg      = "#00000000",
        x       = s.geometry.x + s.geometry.width - offsetx,
        y       = s.geometry.y + (s.geometry.height - offsety) / 2,
      }

    -- Put Items in shaped Container
    volumeOverlays[s.index]:setup
      { -- Container
        { wibox.container.rotate(vol_osd, "east"),
          layout = wibox.layout.fixed.vertical,
        },
        -- Reat Background Color
        bg = beautiful.bg_normal,
        -- Real Anti-Aliased Shape
        shape = gears.shape.rounded_rect,
        widget = wibox.container.background()
      }
  end
)

local hideTimer = gears.timer
  { timeout = 5,
    autostart = true,
    callback =
      function()
        for i, osd in pairs(volumeOverlays) do
          osd.visible = false
        end
      end,
  }

-- ===================================================================
-- External usage
-- ===================================================================

function toggleVolOSD(state)
  local i = awful.screen.focused().index
  volumeOverlays[i].visible = state
  if state then
    for j, osd in pairs(volumeOverlays) do
      if j ~= i then
        osd.visible = false
      end
    end
    hideTimer:again()
    if toggleBriOSD ~= nil then
      toggleBriOSD(false)
    end
  else
    hideTimer:stop()
  end
end
