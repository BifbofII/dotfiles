-- ┌─────────────────────┐
-- │ Awesome Exit Screen │
-- └─────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local icons = require("icons")

-- Default Apps
local apps = require("apps")

-- Custom Widgets
local clickable_container = require("widgets.clickable_container")

local dpi = beautiful.xresources.apply_dpi

-- ===================================================================
-- Appearance
-- ===================================================================

local icon_size = dpi(90)

local buildButton = function(icon)
  local abutton = wibox.widget
    { wibox.widget
      { wibox.widget
        { wibox.widget
          { image  = icon,
            widget = wibox.widget.imagebox,
          },
          margins = dpi(16),
          widget  = wibox.container.margin
        },
        shape         = gears.shape.circle,
        forced_width  = icon_size,
        forced_height = icon_size,
        widget        = clickable_container,
      },
      left   = dpi(24),
      right  = dpi(24),
      widget = wibox.container.margin,
    }
  return abutton
end

-- ===================================================================
-- Functionality
-- ===================================================================

function suspend_command()
  exit_screen_hide()
  awful.spawn.with_shell("systemctl suspend")
end

function exit_command()
  awesome.quit()
end

function lock_command()
  exit_screen_hide()
  awful.spawn.with_shell(apps.lock)
end

function poweroff_command()
  awful.spawn.with_shell("systemctl poweroff -i")
  awful.keygrapper.stop(exit_screen_grabber)
end

function reboot_command()
  awful.spawn.with_shell("systemctl reboot")
  awful.keygrabber.stop(exit_screen_grabber)
end

local poweroff = buildButton(icons.power, "Shutdown")
poweroff:connect_signal("button::release", poweroff_command)

local reboot = buildButton(icons.restart, "Restart")
reboot:connect_signal("button::release", reboot_command)

local suspend = buildButton(icons.sleep, "Sleep")
suspend:connect_signal("button::release", suspend_command)

local exit = buildButton(icons.logout, "Logout")
exit:connect_signal("button::release", exit_command)

local lock = buildButton(icons.lock, "Lock")
lock:connect_signal("button::release", lock_command)

-- ===================================================================
-- Create Widget
-- ===================================================================

local screen_geometry = screen.primary.geometry

-- Create Widget
exit_screen = wibox(
  { x = screen_geometry.x,
    y = screen_geometry.y,
    visible = false,
    ontop = true,
    type = "splash",
    height = screen_geometry.height,
    width = screen_geometry.width,
  }
)

-- FG and BG
exit_screen.bg = beautiful.bg_normal
exit_screen.fg = '#FEFEFE'

local exit_screen_grabber

function exit_screen_hide()
  awful.keygrabber.stop(exit_screen_grabber)
  exit_screen.visible = false
end

function exit_screen_show()
  exit_screen_grabber = awful.keygrabber.run(
    function(_, key, event)
      if event == "release" then return end

      if key == "s" then
        suspend_command()
      elseif key == "e" then
        exit_command()
      elseif key == "l" then
        lock_command()
      elseif key == "p" then
        poweroff_command()
      elseif key == "r" then
        reboot_command()
      elseif key == "Escape" or key == "q" or key == "x" then
        exit_screen_hide()
      end
    end
  )
  exit_screen.visible = true
end

exit_screen:buttons(gears.table.join(
  -- Middle Click: Hide exit_screen
  awful.button({ }, 2, exit_screen_hide),
  -- Right Click: Hide exit_screen
  awful.button({ }, 3, exit_screen_hide)
))

-- Item Placement
exit_screen:setup
  { nil,
    { nil,
      { poweroff,
        reboot,
        suspend,
        exit,
        lock,
        layout = wibox.layout.fixed.horizontal
      },
      nil,
      expand = "none",
      layout = wibox.layout.align.horizontal
    },
    nil,
    expand = "none",
    layout = wibox.layout.align.vertical
  }
