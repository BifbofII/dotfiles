-- ┌────────────────────────────┐
-- │ Brightness OSD for Awesome │
-- └────────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")

-- Custom Widgets
local bri_osd = require("widgets.brightness_slider_osd")

local dpi = beautiful.xresources.apply_dpi

-- ===================================================================
-- Add Brightness Overlay to Each Screen
-- ===================================================================

brightnessOverlays = {}

screen.connect_signal("request::desktop_decoration",
  function(s)
    -- Create Box
    local offsetx = dpi(56, s)
    local offsety = dpi(300, s)

    brightnessOverlays[s.index] = wibox
      { visible = nil,
        screen  = s,
        ontop   = true,
        type    = "normal",
        height  = offsety,
        width   = dpi(48, s),
        bg      = "#00000000",
        x       = s.geometry.x + s.geometry.width - offsetx,
        y       = s.geometry.y + (s.geometry.height - offsety) / 2,
      }

    -- Put Items in shaped Container
    brightnessOverlays[s.index]:setup
      { -- Container
        { wibox.container.rotate(bri_osd, "east"),
          layout = wibox.layout.fixed.vertical,
        },
        -- Reat Background Color
        bg = beautiful.bg_normal,
        -- Real Anti-Aliased Shape
        shape = gears.shape.rounded_rect,
        widget = wibox.container.background()
      }
  end
)

local hideTimer = gears.timer
  { timeout = 5,
    autostart = true,
    callback =
      function()
        for i, osd in pairs(brightnessOverlays) do
          osd.visible = false
        end
      end,
  }

-- ===================================================================
-- External usage
-- ===================================================================

function toggleBriOSD(state)
  local i = awful.screen.focused().index
  brightnessOverlays[i].visible = state
  if state then
    for j, osd in pairs(brightnessOverlays) do
      if j ~= i then
        osd.visible = false
      end
    end
    hideTimer:again()
    if toggleVolOSD ~= nil then
      toggleVolOSD(false)
    end
  else
    hideTimer:stop()
  end
end
