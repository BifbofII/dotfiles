-- ┌──────────────────┐
-- │ Awesome Titlebar │
-- └──────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local gears = require("gears")
local awful = require("awful")
local beautiful = require("beautiful")
local wibox = require("wibox")

local dpi = beautiful.xresources.apply_dpi

-- ===================================================================
-- Functionality
-- ===================================================================

function double_click_event_handler(double_click_event)
  if double_click_timer then
    double_click_timer:stop()
    double_click_timer = nil

    double_click_event()

    return
  end

  double_click_timer = gears.timer.start_new(0.2,
    function()
      double_click_timer = nil
      return false
    end
  )
end

client.connect_signal("request::titlebars",
  function(c)
    -- Buttons for moving/resizing functionality
    local buttons = gears.table.join(
      awful.button({ }, 1,
        function()
          double_click_event_handler(
            function()
              if c.floating then
                c.floating = false
                return
              end

              c.maximized = not c.maximized
              c:raise()
            end
          )
          c:activate { context = "titlebar", action = "mouse_resize" }
        end
      ),
      awful.button({ }, 3, function() c:activate { context = "titlebar", action = "mouse_resize" } end)
    )

    local function decorate_titlebar(c, pos, bg, size)
      c.titlebar_position = pos
      if pos == "left" or pos == "right" then
        -- Create left or right titlebars
        awful.titlebar(c, { position = pos, bg = bg, size = size or beautiful.titlebar_size }):setup
          { { { awful.titlebar.widget.closebutton(c),
                awful.titlebar.widget.maximizedbutton(c),
                awful.titlebar.widget.minimizebutton(c),
                spacing = dpi(7),
                layout  = wibox.layout.fixed.vertical,
              },
              margins = dpi(10),
              widget  = wibox.container.margin,
            },
            { buttons = buttons,
              layout  = wibox.layout.flex.vertical,
            },
            { awful.titlebar.widget.floatingbutton(c),
              margins = dpi(10),
              widget  = wibox.container.margin,
            },
            layout = wibox.layout.align.vertical
          }
      elseif pos == "top" or pos == "bottom" then
        -- Create top or bottom titlebars
        awful.titlebar(c, { position = pos, bg = bg, size = size or beautiful.titlebar_size }):setup
          { { { awful.titlebar.widget.closebutton(c),
                awful.titlebar.widget.maximizedbutton(c),
                awful.titlebar.widget.minimizebutton(c),
                spacing = dpi(7),
                layout  = wibox.layout.fixed.horizontal,
              },
              margins = dpi(10),
              widget  = wibox.container.margin,
            },
            { buttons = buttons,
              layout  = wibox.layout.flex.horizontal,
            },
            { awful.titlebar.wirdget.floatingbutton(c),
              margins = dpi(10),
              widget  = wibox.container.margin,
            },
            layout = wibox.layout.align.horizontal,
          }
      else
        -- Create default left titlebar
        awful.titlebar(c, { position = "left", size = beautiful.titlebar_size }):setup
          { { { awful.titlebar.widget.closebutton(c),
                awful.titlebar.widget.maximizedbutton(c),
                awful.titlebar.widget.minimizebutton(c),
                spacing = dpi(7),
                layout  = wibox.layout.fixed.vertical,
              },
              margins = dpi(10),
              widget  = wibox.container.margin,
            },
            { buttons = buttons,
              layout  = wibox.layout.flex.vertical,
            },
            { awful.titlebar.widget.floatingbutton(c),
              margins = dpi(10),
              widget  = wibox.container.margin,
            },
            layout = wibox.layout.align.vertical
          }
      end
    end

    -- +++++++++++++++++++++++++++++++++++++++++
    -- Set titlebars for clients
    -- +++++++++++++++++++++++++++++++++++++++++
    
    -- Nemo
    if c.class == "Nemo" then
      decorate_titlebar(c, "left", beautiful.background, beautiful.titlebar_size)
    -- Default
    else
      decorate_titlebar(c)
    end
  end
)

local function showTitleBar(c, state)
  -- No titlebars
  -- local is_valid = pcall(function() return c.valid end) and c.valid
  -- if is_valid then
  --   if state then
  --     awful.titlebar.show(c, c.titlebar_position or "left")
  --   else
  --     awful.titlebar.hide(c, c.titlebar_position or "left")
  --   end
  -- end
end

local function clientCallback(c)
  if c.request_no_titlebar then
    gears.timer.delayed_call(function () showTitleBar(c, false) end)
    return
  elseif (c.floating and not c.maximized) or (c.first_tag and c.first_tag.layout == awful.layout.suit.floating) then
    if c.fullscreen then
      showTitleBar(c, true)
    else
      gears.timer.delayed_call(function () showTitleBar(c, true) end)
    end
    return
  end
  gears.timer.delayed_call(function () showTitleBar(c, false) end)
end

-- ===================================================================
-- Signals
-- ===================================================================

client.connect_signal("request::manage", clientCallback)

client.connect_signal("property::floating", clientCallback)

client.connect_signal("property::fullscreen", clientCallback)

tag.connect_signal("property::layout",
  function(t)
    for _, c in pairs(t:clients()) do
      if c.floating or t.layout == awful.layout.suit.floating then
        showTitleBar(c, true)
      else
        showTitleBar(c, false)
      end
    end
  end
)
