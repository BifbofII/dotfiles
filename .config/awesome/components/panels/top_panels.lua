-- ┌────────────────────────┐
-- │ Top Panels for Awesome │
-- └────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local awful = require("awful")

-- Custom Modules
local apps = require("apps")

-- Custom Widgets
local icon_button = require("widgets.icon_button")
local icon_item = require("widgets.icon")
local layout_box = require("widgets.layout_box")
local battery = require("widgets.battery")
local systemtray = require("widgets.systemtray")
local task_list = require("widgets.task_list")
local bluetooth = require("widgets.bluetooth")
local networking = require("widgets.networking")
--local dnd = require("widgets.dnd")

-- Custom Icons
local icons = require("icons")

local dpi = beautiful.xresources.apply_dpi

top_panels = { }

-- ===================================================================
-- Widgets
-- ===================================================================

-- +++++++++++++++++++++++++++++++++++++++++
-- Clock
-- +++++++++++++++++++++++++++++++++++++++++

local textclock = wibox.widget.textclock("<span font='" .. beautiful.title_font .. "'>%k:%M</span>", 1)
local clock_widget = wibox.container.margin(textclock, dpi(0), dpi(0))

-- Clock Tooltip
awful.tooltip(
  { objects = { clock_widget },
    mode  = "outside",
    align = "right",
    timer_function =
      function()
        return os.date("%A, %d.%m.%Y")
      end,
    preferred_positions = { "right", "left", "top", "bottom" },
    margins             = dpi(8),
  }
)

-- +++++++++++++++++++++++++++++++++++++++++
-- Calendar
-- +++++++++++++++++++++++++++++++++++++++++

local cal_shape =
  function(cr, width, height)
    gears.shape.partially_rounded_rect(cr, width, height, false, false, true, true, 12)
  end

local month_calendar = awful.widget.calendar_popup.month(
  { start_sunday  = false,
    spacing       = 10,
    font          = beautiful.titel_font,
    long_weekdays = true,
    margin        = 0,
    style_month   = { border_width = 0, padding = 12, shape = cal_shape, padding = 25 },
    style_header  = { border_width = 0, bg_color = "#00000000" },
    style_weekday = { border_width = 0, bg_color = "#00000000" },
    style_normal  = { border_width = 0, bg_color = "#00000000" },
    style_focus   = { border_width = 0, bg_color = "#8AB4F8" },
  }
)

-- Attach Calendar to Clock
month_calendar:attach(clock_widget, "tc", { on_pressen = true, on_hover = false })

-- +++++++++++++++++++++++++++++++++++++++++
-- Open Default App Button
-- +++++++++++++++++++++++++++++++++++++++++

local add_button = icon_button(icon_item(icons.open, dpi(10)))

add_button:buttons(gears.table.join(awful.button({ }, 1, nil, start_default_app)))

-- ===================================================================
-- Main Top Panel
-- ===================================================================

top_panels.main =
  function(s)
    local panel = wibox
      { ontop   = true,
        screen  = s,
        visible = true,
        height  = dpi(26),
        width   = s.geometry.width,
        x       = s.geometry.x,
        y       = s.geometry.y,
        stretch = false,
        bg      = beautiful.bg_normal,
        fg      = beautiful.fg_normal,
      }

    -- Define Space to push Clients down (equal to Panel Height)
    panel:struts({ top = dpi(26) })

    panel:setup
      { expand = "none",
        layout = wibox.layout.align.horizontal,
        { layout = wibox.layout.fixed.horizontal,
          add_button,
          task_list(s),
        },
        clock_widget,
        { layout = wibox.layout.fixed.horizontal,
          systemtray(s),
          --dnd(),
          bluetooth(),
          networking(),
          battery(),
          layout_box(s),
        },
      }

    return panel
  end

  return top_panels
