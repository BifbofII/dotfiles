-- ┌─────────────────────────┐
-- │ Side Panels for Awesome │
-- └─────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local awful = require("awful")

-- Custom Widgets
local tag_list = require("widgets.tag_list")
local folders = require("widgets.folders")

local dpi = beautiful.xresources.apply_dpi

side_panels = { }

-- ===================================================================
-- Bar Creation
-- ===================================================================

local icon_size = dpi(55)
local separator_size = dpi(16)

side_panels.left =
  function(s)
    local optimal_height = 14 * icon_size + 2 * separator_size
    local content =
      { fill_space = false,
        layout = wibox.layout.fixed.vertical,
        -- Taglist Widget
        tag_list(s),
        -- Folder Widgets
        folders.separator,
        folders.home,
        folders.documents,
        folders.downloads,
        folders.separator,
        folders.trash,
      }

    if 14 * icon_size + 2 * separator_size > s.geometry.height - dpi(26) then
      optimal_height = 10 * icon_size
      content = tag_list(s)
    end

    local left_panel = awful.wibar(
      { position       = "left",
        screen         = s,
        width          = icon_size,
        height         = optimal_height,
        stretch        = false,
        shape          =
          function(cr, width, height)
            gears.shape.partially_rounded_rect(cr, width, height, false, true, true, false, 12)
          end,
      }
    )

    left_panel:setup
      { layout = wibox.layout.align.vertical,
        expand = "none",
        nil,
        content,
        nil,
      }

    left_panel.optimal_height = left_panel.height

    s.side_panel = left_panel

    return left_panel
  end

return side_panels
