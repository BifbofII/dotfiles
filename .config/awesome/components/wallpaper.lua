-- ┌──────────────────────────────┐
-- │ Wallpaper Config for Awesome │
-- └──────────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local awful = require("awful")
local gears = require("gears")
local naughts = require("naughty")
local beautiful = require("beautiful")

