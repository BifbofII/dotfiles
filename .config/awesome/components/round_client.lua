-- ┌─────────────────────────┐
-- │ Awesome Rounded Clients │
-- └─────────────────────────┘

-- ===================================================================
-- Initialization
-- ===================================================================

-- Awesome Modules
local gears = require("gears")
local awful = require("awful")
local beautiful = require("beautiful")

-- ===================================================================
-- Functionality
-- ===================================================================

local function renderClient(client)
  if client.fullscreen or client.maximized or client.first_tag.layout == awful.layout.suit.max then
    client.shape = gears.shape.rectangle
  else
    client.shape =
      function(cr, w, h)
        gears.shape.rounded_rect(cr, w, h, beautiful.corner_radius)
      end
  end
end

local changes_on_screen_called = false

local function changesOnScreen(current_screen)
  local tag_is_max =
    current_screen.selected_tag ~= nil and current_screen.selected_tag.layout == awful.layout.suit.max
  local clients_to_manage = { }

  for _, client in pairs(current_screen.clients) do
    if not client.skip_decoration and not client.hidden then
      table.insert(clients_to_manage, client)
    end
  end

  for _, client in pairs(clients_to_manage) do
    renderClient(client)
  end

  changes_on_screen_called = false
end

local function clientCallback(client)
  if not changes_on_screen_called then
    if not client.skip_decoration and client.screen then
      changes_on_screen_called = true
      local screen = client.screen
      gears.timer.delayed_call(function() changesOnScreen(screen) end)
    end
  end
end

local function tagCallback(tag)
  if not changes_on_screen_called then
    if tag.screen then
      changes_on_screen_called = true
      local screen = tag.screen
      gears.timer.delayed_call(function() changesOnScreen(screen) end)
    end
  end
end

client.connect_signal("manage", clientCallback)

client.connect_signal("unmanage", clientCallback)

client.connect_signal("property::hidden", clientCallback)

client.connect_signal("property::maximized", clientCallback)

client.connect_signal("property::minimized", clientCallback)

client.connect_signal("property::fullscreen",
  function(c)
    if c.fullscreen then
      renderClient(c)
    else
      clientCallback(c)
    end
  end
)

tag.connect_signal("property::selected", tagCallback)

tag.connect_signal("property::layout", tagCallback)
