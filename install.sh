#!/usr/bin/bash

EVAL=0
STOW=true

if ! type wezterm &> /dev/null; then
  >&2 echo "wezterm not found, make sure to install it"
  EVAL=$((${EVAL}+1))
fi

if ! type starship &> /dev/null; then
  >&2 echo "Starship prompt not found, make sure to install it"
  >&2 echo "  https://starship.rs/"
  EVAL=$((${EVAL}+1))
fi

NVIM=true
if ! test nvim &> /dev/null; then
  >&2 echo "NeoVim not found, make sure to install it"
  EVAL=$((${EVAL}+1))
  NVIM=false
fi

if ! type joshuto &> /dev/null; then
  >&2 echo "joshuto not found, make sure to install it"
  >&2 echo "  https://github.com/kamiyaa/joshuto"
  EVAL=$((${EVAL}+1))
fi

if ! type zoxide &> /dev/null; then
  >&2 echo "zoxide not found, make sure to install it"
  >&2 echo "  https://github.com/ajeetdsouza/zoxide"
  EVAL=$((${EVAL}+1))
fi

if ! type eza &> /dev/null; then
  >&2 echo "eza not found, make sure to install it"
  >&2 echo "  https://github.com/eza-community/eza"
  EVAL=$((${EVAL}+1))
fi

if ! type bat &> /dev/null; then
  >&2 echo "bat not found, make sure to install it"
  >&2 echo "  https://github.com/sharkdp/bat"
  EVAL=$((${EVAL}+1))
fi

if ! type rg &> /dev/null; then
  >&2 echo "ripgrep not found, make sure to install it"
  >&2 echo "  https://github.com/BurntSushi/ripgrep"
  EVAL=$((${EVAL}+1))
fi

if ! type fzf &> /dev/null; then
  >&2 echo "fzf not found, make sure to install it"
  EVAL=$((${EVAL}+1))
fi

if ! type tmux &> /dev/null; then
  >&2 echo "tmux not found, make sure to install it"
  EVAL=$((${EVAL}+1))
fi

if [ ! -d ${HOME}/.config/tmux/plugins/tpm/.git ] || \
  [ "$(git --git-dir=${HOME}/.config/tmux/plugins/tpm/.git remote get-url origin)" != "https://github.com/tmux-plugins/tpm" ] ; then
  >&2 echo "Tmux plugin manager not installed, make sure to install it"
  >&2 echo "  https://github.com/tmux-plugins/tpm"
  EVAL=$((${EVAL}+1))
  # create empty plugins directly to prevent stow from linking the entire folder
  mkdir -p "~/.config/tmux/plugins"
fi

if type stow &> /dev/null && ${STOW}; then
  stow . || exit 42
else
  ${STOW} && >&2 echo "GNU stow not found, install it or manually install the dotfiles"
  ${STOW} && EVAL=$((${EVAL}+1))
fi

exit ${EVAL}
